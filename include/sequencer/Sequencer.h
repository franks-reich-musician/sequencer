#ifndef FR_MUSICIAN_SEQUENCER_SEQUENCER_H
#define FR_MUSICIAN_SEQUENCER_SEQUENCER_H


#include <map>
#include <memory>


#include <server/IPCServer.h>


#include "shared/EntityContainer.h"


namespace fr::musician::sequencer {


class Sequencer {
  public:
    Sequencer();


  private:
    songs_type _songs;
    tracks_type _tracks;
    patterns_type _patterns;
    events_type _events;
    signal_tracks_type _signalTracks;
    signals_type _signals;
    fr::musician::ipc_protocol::server::IPCServer _server;

    void _registerHandlerFunctions();
};


} // end of namespace fr::musician::sequencer


#endif
