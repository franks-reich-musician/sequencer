#ifndef FR_MUSICIAN_SEQUENCER_TYPEDEF_H
#define FR_MUSICIAN_SEQUENCER_TYPEDEF_H


#include <tuple>


#include <events/Notes.pb.h>


namespace fr::musician::sequencer {


using sequencer_key_type = unsigned int;
using sequencer_time_type = unsigned int;
using sequencer_octave_type = unsigned int;
using sequencer_interval = std::tuple<sequencer_time_type, sequencer_time_type>;
using sequencer_note_event_interval = std::tuple<
  sequencer_key_type,
  sequencer_key_type,
  sequencer_octave_type,
  fr::musician::data_definition::events::Notes>;


const std::string REQUEST_QUEUE_NAME = "fr_musician_request_queue";


} // end of namespace fr::musician::sequencer


#endif
