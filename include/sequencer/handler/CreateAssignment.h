#ifndef FR_MUSICIAN_SEQUENCER_CREATE_ASSIGNMENT_H
#define FR_MUSICIAN_SEQUENCER_CREATE_ASSIGNMENT_H


#include "../Typedef.h"
#include "Exceptions.h"


namespace fr::musician::sequencer::handler {


template<class ParentContainer, class ChildContainer>
class CreateAssignment {
  public:
    CreateAssignment(ParentContainer &parents, ChildContainer &children):
      _parents(parents),
      _children(children)
    {}

    using AssignmentAttributes =
      typename ParentContainer::mapped_type::assignment_attributes_type;
    using ParentAttributes =
      typename ParentContainer::mapped_type::attributes_type;
    using ChildAttributes =
      typename ChildContainer::mapped_type::attributes_type;

    void operator()(
      sequencer_key_type childId,
      sequencer_key_type parentId,
      const AssignmentAttributes &attributes,
      ChildAttributes &childAttributes,
      ParentAttributes &parentAttributes,
      AssignmentAttributes &responseAttributes)
    {
      locking::read_lock childContainerLock(*_children.mutex, std::defer_lock);
      locking::read_lock parentContainerLock(*_parents.mutex, std::defer_lock);
      std::lock(childContainerLock, parentContainerLock);

      const auto child = _children.container.find(childId);
      if (child == _children.container.end()) {
        throw child_range_error("Child does not exist");
      }
      childAttributes = child->second.attributes;

      const auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist");
      }
      parentAttributes = parent->second.attributes;

      locking::write_lock parentLock(*parent->second.mutexes.container);
      parent->second.container.insert(
        ParentContainer::mapped_type::value_type(childId, attributes));
      responseAttributes = attributes;
    }


  private:
    ParentContainer &_parents;
    ChildContainer &_children;
};


} // end of namespace fr::musician::sequencer::handler


#endif
