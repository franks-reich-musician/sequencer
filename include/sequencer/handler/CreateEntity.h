#ifndef FR_MUSICIAN_SEQUENCER_CREATE_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_CREATE_ENTITY_H


#include <atomic>
#include <utility>


#include "../Typedef.h"


namespace fr::musician::sequencer::handler {


template <class EntityContainerType>
class CreateEntity {
  public:
    explicit CreateEntity(EntityContainerType &container):
      _container(container)
      {}

    using AttributesType =
      typename EntityContainerType::mapped_type::attributes_type;

    void operator() (
      const AttributesType &requestAttributes,
      AttributesType &responseAttributes)
    {
      auto entity = _create(requestAttributes);
      _respond(entity, responseAttributes);
      _insert(entity);
    }

    sequencer_key_type nextId() {
      return _nextId;
    }


  private:
    static std::atomic<sequencer_key_type> _nextId;
    EntityContainerType &_container;

    typename EntityContainerType::mapped_type _create(
      const AttributesType &requestAttributes)
    {
      auto entity = EntityContainerType::mapped_type();
      entity.attributes = requestAttributes;
      auto id = _nextId.fetch_add(1);
      entity.attributes.set_id(id);
      return entity;
    }

    void _insert(typename EntityContainerType::mapped_type &entity) {
      const auto id = entity.attributes.id();
      EntityContainerType::value_type value(id, std::move(entity));
      locking::write_lock lock(*_container.mutex);
      _container.container.insert(std::move(value));
    }

    void _respond(
      const typename EntityContainerType::mapped_type &entity,
      AttributesType &responseAttributes)
    {
      responseAttributes = entity.attributes;
    }
};


template <class EntityContainerType>
std::atomic<sequencer_key_type> CreateEntity<EntityContainerType>::_nextId = 0;


} // end of namespace fr::musician::sequencer::handler


#endif
