#ifndef FR_MUSICIAN_SEQUENCER_DELETE_ASSIGNMENT_H
#define FR_MUSICIAN_SEQUENCER_DELETE_ASSIGNMENT_H


#include <algorithm>


#include "../Typedef.h"
#include "Exceptions.h"


namespace fr::musician::sequencer::handler {


template <class ParentContainer>
class DeleteAssignment {
  public:
    DeleteAssignment(ParentContainer &parents):
      _parents(parents)
    {}

    void operator()(
      const sequencer_key_type childId, const sequencer_key_type parentId) {
      locking::read_lock parentContainerLock(*_parents.mutex);

      const auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist");
      }

      locking::write_lock parentLock(*parent->second.mutexes.container);
      const auto assignment = parent->second.container.find(childId);
      if (assignment == parent->second.container.end()) {
        throw assignment_range_error("Assignment for child does not exist");
      }

      parent->second.container.erase(assignment);
    }


  private:
    ParentContainer &_parents;
};


} // end of namespace fr::musician::sequencer::handler


#endif
