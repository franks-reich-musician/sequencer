#ifndef FR_MUSICIAN_SEQUENCER_DELETE_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_DELETE_ENTITY_H


#include <stdexcept>


#include "../Typedef.h"


namespace fr::musician::sequencer::handler {


template <class EntityContainerType>
class DeleteEntity {
  public:
    explicit DeleteEntity(EntityContainerType &container):
      _container(container)
      {}

    void operator() (
      const sequencer_key_type id)
    {
      locking::write_lock container_lock(*_container.mutex);
      auto entity = _container.container.find(id);
      if (entity == _container.container.end()) {
        throw std::range_error("Invalid key");
      }
      _container.container.erase(entity);
    }


  private:
    EntityContainerType &_container;
};


} // end of fr::musician::sequencer::handler


#endif
