#ifndef FR_MUSICIAN_SEQUENCER_HANDLER_ERROR_MESSAGE_H
#define FR_MUSICIAN_SEQUENCER_HANDLER_ERROR_MESSAGE_H


#include <string>


#include <Response.pb.h>
#include "../Typedef.h"


namespace fr::musician::sequencer::handler::error {


void prepareDoesNotExistErrorMessage(
  fr::musician::ipc_protocol::Response &response,
  std::string entity,
  fr::musician::ipc_protocol::Error_ErrorCode errorCode);


void preparePositionWouldOverlapErrorMessage(
  fr::musician::ipc_protocol::Response &response,
  std::string child,
  fr::musician::ipc_protocol::Error_ErrorCode errorCode);


void prepareNegativeRangeErrorMessage(
  fr::musician::ipc_protocol::Response &response,
  std::string child);


void preparePositionBaseErrorMessage(
  fr::musician::ipc_protocol::Response &response,
  std::string position,
  fr::musician::ipc_protocol::Error_ErrorCode errorCode);


}


#endif
