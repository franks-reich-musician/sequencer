#ifndef FR_MUSICIAN_SEQUENCER_EXCEPTION_H
#define FR_MUSICIAN_SEQUENCER_EXCEPTION_H


#include <stdexcept>


namespace fr::musician::sequencer::handler {


class child_range_error: public std::range_error {
  public:
    child_range_error(const std::string &_Message):
      range_error(_Message)
    {}
};


class parent_range_error: public std::range_error {
  public:
    parent_range_error(const std::string &_Message):
      range_error(_Message)
    {}
};


class assignment_range_error: public std::range_error {
  public:
    assignment_range_error(const std::string &_Message):
      range_error(_Message)
    {}
};


class begin_base_mismatch_error: public std::runtime_error {
  public:
    begin_base_mismatch_error(const std::string &_Message):
      runtime_error(_Message)
    {}
};


class end_base_mismatch_error: public std::runtime_error {
  public:
    end_base_mismatch_error(const std::string &_Message):
      runtime_error(_Message)
    {}
};


class overlapping_schedule_error: public std::runtime_error {
  public:
    overlapping_schedule_error(const std::string &_Message):
      runtime_error(_Message)
    {}
};


class negative_length_error: public std::runtime_error {
  public:
    negative_length_error(const std::string &_Message):
      runtime_error(_Message)
    {}
};


class child_not_scheduled_at_position: public std::runtime_error {
  public:
    child_not_scheduled_at_position(const std::string &_Message):
      runtime_error(_Message)
    {}
};


} // end of namespace fr::musician::sequencer::handler


#endif
