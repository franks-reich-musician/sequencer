#ifndef FR_MUSICIAN_SEQUENCER_GET_ALL_ASSIGNMENT_IDS_H
#define FR_MUSICIAN_SEQUENCER_GET_ALL_ASSIGNMENT_IDS_H


#include <vector>
#include <algorithm>


#include "../Typedef.h"
#include "Exceptions.h"


namespace fr::musician::sequencer::handler {


template <class ParentContainer>
class GetAllAssignmentIds {
  public:
    GetAllAssignmentIds(ParentContainer &parents):
      _parents(parents)
    {}

    void operator()(
      sequencer_key_type parentId,
      std::vector<sequencer_key_type> &ids)
    {
      locking::read_lock parentContainerLock(*_parents.mutex);
      const auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist");
      }

      locking::read_lock assignmentContainerLock(
        *parent->second.mutexes.container);
      std::transform(
        parent->second.container.begin(),
        parent->second.container.end(),
        std::back_inserter(ids),
        [](const auto &assignment) { return assignment.first; });
    }


  private:
    ParentContainer &_parents;
};


}


#endif
