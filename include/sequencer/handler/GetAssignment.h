#ifndef FR_MUSICIAN_SEQUENCER_GET_ASSIGNMENT_H
#define FR_MUSICIAN_SEQUENCER_GET_ASSIGNMENT_H


#include "../Typedef.h"
#include "Exceptions.h"


namespace fr::musician::sequencer::handler {


template <class ParentContainer, class ChildContainer>
class GetAssignment {
  public:
    GetAssignment(ParentContainer &parents, ChildContainer &children):
      _parents(parents),
      _children(children)
    {}

    using ChildAttributes =
      typename ChildContainer::mapped_type::attributes_type;

    void operator()(
      sequencer_key_type childId,
      sequencer_key_type parentId,
      ChildAttributes &child,
      typename
        ParentContainer::mapped_type::assignment_attributes_type &attributes)
    {
      _fillChildInfo(childId, child);
      _fillAssignmentInfo(childId, parentId, attributes);
    }


  private:
    ParentContainer &_parents;
    ChildContainer &_children;

    void _fillAssignmentInfo(
      sequencer_key_type childId,
      sequencer_key_type parentId,
      typename
        ParentContainer::mapped_type::assignment_attributes_type &attributes)
    {
      locking::read_lock containerLock(*_children.mutex, std::defer_lock);
      const auto parentIter = _parents.container.find(parentId);
      if (parentIter == _parents.container.end()) {
        throw parent_range_error("Parent does not exist");
      }

      locking::read_lock assignmentLock(*parentIter->second.mutexes.container);
      const auto assignmentIter = parentIter->second.container.find(childId);
      if (assignmentIter == parentIter->second.container.end()) {
        throw assignment_range_error("Assignment does not exist.");
      }

      locking::read_lock assignmentAttributesLock(
        *assignmentIter->second.mutex);
      attributes = assignmentIter->second.attributes;
    }

    void _fillChildInfo(
      sequencer_key_type childId,
      typename ChildContainer::mapped_type::attributes_type &child)
    {
      locking::read_lock containerLock(*_children.mutex, std::defer_lock);
      const auto childIter = _children.container.find(childId);
      if (childIter == _children.container.end()) {
        throw child_range_error("Child does not exist");
      }

      locking::read_lock attributesLock(*childIter->second.mutexes.attributes);
      child = childIter->second.attributes;
    }
};


} // end of namespace fr::musician::sequencer::handler


#endif
