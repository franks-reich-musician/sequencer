#ifndef FR_MUSICIAN_SEQUENCER_GET_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_GET_ENTITY_H


#include <stdexcept>


#include "../Typedef.h"


namespace fr::musician::sequencer::handler {


template <class EntityContainerType>
class GetEntity {
  public:
    explicit GetEntity(EntityContainerType &container):
      _container(container)
      {}

    using AttributesType =
      typename EntityContainerType::mapped_type::attributes_type;

    void operator()(sequencer_key_type id, AttributesType &responseAttributes) {
      locking::read_lock container_lock(*_container.mutex);
      auto entity = _container.container.find(id);
      if (entity == _container.container.end()) {
        throw std::range_error("Invalid key");
      }
      locking::read_lock entity_lock(*entity->second.mutexes.attributes);
      responseAttributes = entity->second.attributes;
    }


  private:
    EntityContainerType &_container;
};

} // end of namespace fr::musician::sequencer::handler


#endif
