#ifndef FR_MUSICIAN_SEQUENCER_GET_SCHEDULED_ENTITY_POSITIONS_AND_IDS_H
#define FR_MUSICIAN_SEQUENCER_GET_SCHEDULED_ENTITY_POSITIONS_AND_IDS_H


#include <events/Notes.pb.h>


#include "../Typedef.h"


namespace fr::musician::sequencer::handler {


template <class ParentContainer>
class GetScheduledEntityPositionsAndIds {
  public:
    GetScheduledEntityPositionsAndIds(ParentContainer &parents):
      _parents(parents)
    {}

    typedef typename ParentContainer::mapped_type::key_type position_type;
    typedef std::pair<position_type, sequencer_key_type> result_type;
    typedef std::vector<result_type> results_type;

    void operator()(
      sequencer_time_type begin,
      sequencer_time_type end,
      sequencer_key_type parentId,
      typename results_type &results)
    {
      if (begin > end) {
        throw negative_length_error("Begin position is before end position.");
      }

      locking::read_lock parentContainerLock(*_parents.mutex);
      auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist.");
      }

      auto entity = parent->second.container.begin();
      while (
        entity != parent->second.container.end() &&
        std::get<0>(entity->first) < end)
      {
        if (std::get<1>(entity->first) > begin) {
          results.push_back(*entity);
        }
        entity++;
      }
    }


  private:
    ParentContainer &_parents;
};


} // end of namespace fr::musician::sequencer::handler


#endif
