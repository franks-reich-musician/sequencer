#ifndef FR_MUSICIAN_SEQUENCER_HANDLER_LOGGING_H
#define FR_MUSICIAN_SEQUENCER_HANDLER_LOGGING_H


#include <string>


#include "../Typedef.h"


namespace fr::musician::sequencer::handler::logging {


void logScheduleHandling(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logScheduleError(
  std::string child, std::string parent, sequencer_key_type childId,
  sequencer_key_type parentId, std::string errorEntity);
void logOverlappingScheduleError(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logScheduleNegativeRangeError(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logScheduleBaseMismatchError(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId,
  std::string position);

void logAssignHandling(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logAssignError(
  std::string child, std::string parent, sequencer_key_type childId,
  sequencer_key_type parentId, std::string errorEntity);

void logGetAllAssignmentsHandling(
  std::string child, std::string parent, sequencer_key_type parentId);
void logGetAllAssignmentsError(
  std::string child, std::string parent, sequencer_key_type parentId);

void logRemoveAssignmentHandling(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logRemoveAssignmentError(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logRemoveAssignmentParentError(
  std::string parent, sequencer_key_type parentId);

void logUpdateAssignmentHandling(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logUpdateAssignmentError(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);
void logUpdateAssignmentParentError(
  std::string child, std::string parent,
  sequencer_key_type childId, sequencer_key_type parentId);

void logCreateHandling(std::string entity);

void logDeleteHandling(std::string entity, sequencer_key_type);
void logDeleteError(std::string entity, sequencer_key_type);

void logGetHandling(std::string entity, sequencer_key_type);
void logGetError(std::string entity, sequencer_key_type);

void logUpdateHandling(std::string entity, sequencer_key_type);
void logUpdateError(std::string entity, sequencer_key_type);


} // end of namespace fr::musician::sequencer::handler::logging


#endif
