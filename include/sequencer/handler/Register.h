#ifndef FR_MUSICIAN_SEQUENCER_REGISTER_H
#define FR_MUSICIAN_SEQUENCER_REGISTER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::handler {


void registerHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::songs_type &songs,
  fr::musician::sequencer::tracks_type &tracks,
  fr::musician::sequencer::patterns_type &patterns,
  fr::musician::sequencer::events_type &events,
  fr::musician::sequencer::signal_tracks_type &signalTracks,
  fr::musician::sequencer::signals_type &signals);


} // end of namespace fr::musician::sequencer::handler


#endif

