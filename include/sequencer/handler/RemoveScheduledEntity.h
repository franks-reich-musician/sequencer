#ifndef FR_MUSICIAN_SEQUENCER_REMOVE_SCHEDULED_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_REMOVE_SCHEDULED_ENTITY_H


#include "../Typedef.h"


namespace fr::musician::sequencer::handler {


template <class ParentContainer>
class RemoveScheduledEntity {
  public:
    RemoveScheduledEntity(ParentContainer &parents):
      _parents(parents)
    {}

    typedef typename ParentContainer::mapped_type::key_type position_type;

    void operator()(
      sequencer_key_type childId,
      sequencer_key_type parentId,
      const position_type &position)
    {
      constexpr auto beginIndex = 0;
      constexpr auto endIndex = 1;
      const auto beginPosition = std::get<beginIndex>(position);
      const auto endPosition = std::get<endIndex>(position);
      if (endPosition < beginPosition) {
        throw negative_length_error("Begin position is after end position.");
      }

      locking::read_lock parentContainerLock(*_parents.mutex);
      auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist.");
      }

      locking::write_lock parentLock(*parent->second.mutexes.container);
      auto entityToRemove = parent->second.container.find(position);
      if (entityToRemove == parent->second.container.end()) {
        throw child_not_scheduled_at_position(
          "No entity scheduled at given position.");
      } else if (entityToRemove->second != childId) {
        throw child_not_scheduled_at_position(
          "Give entity not scheduled at position.");
      }

      parent->second.container.erase(entityToRemove);
    }

  private:
    ParentContainer &_parents;
};


} // end of namespace fr::musician::sequencer::handler


#endif
