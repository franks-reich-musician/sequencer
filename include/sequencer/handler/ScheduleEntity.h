#ifndef FR_MUSICIAN_SEQUENCER_SCHEDULE_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_SCHEDULE_ENTITY_H


#include "../Typedef.h"
#include "Exceptions.h"
#include "../shared/Locking.h"
#include "../shared/EntityContainer.h"


namespace fr::musician::sequencer::handler {


void checkOverlap(
  const sequencer_note_event_interval &position,
  const typename pattern_type::container_type &container);


void checkOverlap(
  const sequencer_interval &position,
  const typename signal_track_type::container_type &container);


template <class ParentContainer, class ChildContainer>
class ScheduleEntity {
  public:
    ScheduleEntity(ParentContainer &parents, ChildContainer &children):
      _parents(parents),
      _children(children)
    {}

    typedef typename ParentContainer::mapped_type::key_type position_type;

    void operator()(
      const sequencer_key_type childId,
      const sequencer_key_type parentId,
      const typename position_type position)
    {
      constexpr auto beginIndex = 0;
      constexpr auto endIndex = 1;
      const auto beginPosition = std::get<beginIndex>(position);
      const auto endPosition = std::get<endIndex>(position);
      if (endPosition < beginPosition) {
        throw negative_length_error("Begin position is after end position.");
      }

      locking::read_lock childContainerLock(*_children.mutex, std::defer_lock);
      locking::read_lock parentContainerLock(*_parents.mutex, std::defer_lock);
      std::lock(childContainerLock, parentContainerLock);

      if (_children.container.count(childId) == 0) {
        throw child_range_error("Child does not exist.");
      }

      auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist.");
      }

      locking::write_lock parentLock(*parent->second.mutexes.container);
      checkOverlap(position, parent->second.container);
      parent->second.container.insert(std::make_pair(position, childId));
    }


  private:
    ParentContainer &_parents;
    ChildContainer &_children;
};


} // end of namespace fr::musician::sequencer::handler


#endif
