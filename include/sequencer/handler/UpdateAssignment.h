#ifndef FR_MUSICIAN_SEQUENCER_UPDATE_ASSIGNMENT_H
#define FR_MUSICIAN_SEQUENCER_UPDATE_ASSIGNMENT_H


#include <algorithm>


#include "../Typedef.h"
#include "Exceptions.h"


namespace fr::musician::sequencer::handler {


template <class ParentContainer>
class UpdateAssignment {
  public:
    UpdateAssignment(ParentContainer &parents):
      _parents(parents)
    {}

    using AssignmentAttributes =
      typename ParentContainer::mapped_type::assignment_attributes_type;

    void operator()(
      sequencer_key_type childId,
      sequencer_key_type parentId,
      const typename AssignmentAttributes &attributes)
    {
      locking::read_lock containerLock(*_parents.mutex);
      auto parent = _parents.container.find(parentId);
      if (parent == _parents.container.end()) {
        throw parent_range_error("Parent does not exist");
      }

      locking::read_lock parentLock(*parent->second.mutexes.container);
      auto assignment = parent->second.container.find(childId);
      if (assignment == parent->second.container.end()) {
        throw assignment_range_error("Assignment for child does not exist");
      }

      locking::write_lock assignmentLock(*assignment->second.mutex);
      assignment->second.attributes = attributes;
    }


  private:
    ParentContainer &_parents;
};


} // namespace fr::musician::sequencer::handler


#endif
