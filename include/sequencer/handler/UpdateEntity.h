#ifndef FR_MUSICIAN_SEQUENCER_UPDATE_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_UPDATE_ENTITY_H


#include <stdexcept>


#include "../Typedef.h"


namespace fr::musician::sequencer::handler {


template<class EntityContainerType>
class UpdateEntity {
  public:
    UpdateEntity(EntityContainerType &container):
      _container(container)
    {}

    using AttributesType =
      typename EntityContainerType::mapped_type::attributes_type;

    void operator()(
      const AttributesType &requestAttributes,
      AttributesType &responseAttributes)
    {
      responseAttributes = requestAttributes;
      locking::read_lock container_lock(*_container.mutex);
      auto entity = _container.container.find(requestAttributes.id());
      if (entity == _container.container.end()) {
        throw std::range_error("Invalid key");
      }
      locking::write_lock attributes_lock(*entity->second.mutexes.attributes);
      entity->second.attributes = requestAttributes;
    }


  private:
    EntityContainerType &_container;
};


} // end of namespace fr::musician::sequencer::handler
#endif
