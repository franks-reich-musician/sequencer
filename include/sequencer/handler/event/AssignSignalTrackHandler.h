#ifndef FR_MUSICIAN_SEQUENCER_ASSIGN_SIGNAL_TRACK_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_ASSIGN_SIGNAL_TRACK_HANDLER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::event {


void registerAssignSignalTrackHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::events_type &events,
  fr::musician::sequencer::signal_tracks_type &signalTracks);


} // end of namespace fr::musician::sequencer::event


#endif
