#ifndef FR_MUSICIAN_SEQUENCER_DELETE_EVENT_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_DELETE_EVENT_HANDLER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::event {


void registerDeleteEventHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::events_type &events);


} // end of namespace fr::musician::sequencer::event


#endif
