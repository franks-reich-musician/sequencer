#ifndef FR_MUSICIAN_SEQUENCER_CREATE_PATTERN_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_CREATE_PATTERN_HANDLER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::pattern {


void registerCreatePatternHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::patterns_type &patterns);


} // end of namespace fr::musician::sequencer::pattern


#endif
