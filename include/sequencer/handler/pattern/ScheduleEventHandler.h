#ifndef FR_MUSICIAN_SEQUENCER_SCHEDULE_EVENT_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_SCHEDULE_EVENT_HANDLER_H


#include <server/FunctionRegistry.h>
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::pattern {


void registerScheduleEventHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::patterns_type &patterns,
  fr::musician::sequencer::events_type &events);


} // end of namespace fr::musician::sequencer::pattern


#endif
