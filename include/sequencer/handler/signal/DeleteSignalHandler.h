#ifndef FR_MUSICIAN_SEQUENCER_DELETE_SIGNAL_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_DELETE_SIGNAL_HANDLER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::signal {


void registerDeleteSignalHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::signals_type &signals);


} // end of namespace fr::musician::sequencer::signal


#endif
