#ifndef FR_MUSICIAN_SEQUENCER_CREATE_SONG_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_CREATE_SONG_HANDLER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::song {


void registerCreateSongHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::songs_type &songs);


} // end of fr::musician::sequencer::song


#endif
