#ifndef FR_MUSICIAN_SEQUENCER_REMOVE_TRACK_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_REMOVE_TRACK_HANDLER_H


#include <server/FunctionRegistry.h>
#include <sequencer/shared/EntityContainer.h>


#include "sequencer/Typedef.h"


namespace fr::musician::sequencer::song {


void registerRemoveTrackHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::songs_type &songs);


} // end of namespace fr::musician::sequencer::song


#endif
