#ifndef FR_MUSICIAN_SEQUENCER_UPDATE_TRACK_ASSIGNMENT_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_UPDATE_TRACK_ASSIGNMENT_HANDLER_H


#include <server/FunctionRegistry.h>
#include <sequencer/shared/EntityContainer.h>


#include "sequencer/Typedef.h"


namespace fr::musician::sequencer::song {


void registerUpdateTrackAssignmentHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::songs_type &songs,
  fr::musician::sequencer::tracks_type &tracks);


} // end of namespace fr::musician::sequencer::song


#endif
