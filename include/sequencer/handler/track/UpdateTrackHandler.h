#ifndef FR_MUSICIAN_SEQUENCER_UPDATE_TRACK_HANDLER_H
#define FR_MUSICIAN_SEQUENCER_UPDATE_TRACK_HANDLER_H


#include <server/FunctionRegistry.h>


#include "sequencer/Typedef.h"
#include "sequencer/shared/EntityContainer.h"


namespace fr::musician::sequencer::track {


void registerUpdateTrackHandler(
  fr::musician::ipc_protocol::server::FunctionRegistry &registry,
  fr::musician::sequencer::tracks_type &tracks);


} // end of namespace fr::musician::sequencer::pattern


#endif
