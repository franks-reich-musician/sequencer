#ifndef FR_MUSICIAN_SEQUENCER_MUSIC_EXCEPTIONS_H
#define FR_MUSICIAN_SEQUENCER_MUSIC_EXCEPTIONS_H


#include <stdexcept>


namespace fr::musician::sequencer::time {


class denominator_error: public std::runtime_error {
  public:
    denominator_error(const std::string &_Message):
      runtime_error(_Message)
    {}
};


class resolution_error: public std::runtime_error {
  public:
    resolution_error(const std::string &_Message):
      runtime_error(_Message)
    {}
};


class denominator_and_division_count_mismatch: public std::range_error {
  public:
    denominator_and_division_count_mismatch(const std::string _Message):
      range_error(_Message)
    {}
};


} // end of namespace fr::musician::sequencer::time

#endif
