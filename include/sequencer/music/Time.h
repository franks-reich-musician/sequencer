#ifndef FR_MUSICIAN_SEQUENCER_MUSIC_TIME_H
#define FR_MUSICIAN_SEQUENCER_MUSIC_TIME_H


#include <time/MusicTime.pb.h>


#include "../Typedef.h"


namespace fr::musician::sequencer::time {


void checkMusicTime(fr::musician::data_definition::time::MusicTime time);


sequencer_time_type convertMusicTime(
  fr::musician::data_definition::time::MusicTime time);


fr::musician::data_definition::time::MusicTime convertInternalTime(
  sequencer_time_type time,
  sequencer_time_type base,
  sequencer_time_type numberOfDenominators,
  sequencer_time_type resolution);


} // end of namespace fr::musician::sequencer::time


#endif
