#ifndef FR_MUSICIAN_SEQUENCER_SHARED_ASSIGNMENT_H
#define FR_MUSICIAN_SEQUENCER_SHARED_ASSIGNMENT_H


#include "../Typedef.h"
#include "Locking.h"


namespace fr::musician::sequencer {


template <class Attributes>
struct Assignment {
  Assignment(Attributes attributes):
    attributes(attributes),
    mutex(std::make_unique<std::shared_mutex>())
  {}

  locking::mutex_type mutex;
  Attributes attributes;
};


} // end of namespace fr::musician::sequencer


#endif
