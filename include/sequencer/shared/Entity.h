#ifndef FR_MUSICIAN_SEQUENCER_SHARED_ENTITY_H
#define FR_MUSICIAN_SEQUENCER_SHARED_ENTITY_H


#include <tuple>


#include <Song.pb.h>
#include <Signal.pb.h>
#include <SignalTrack.pb.h>
#include <Pattern.pb.h>
#include <Track.pb.h>
#include <Event.pb.h>
#include <assignment/TrackAssignment.pb.h>
#include <assignment/SignalTrackAssignment.pb.h>
#include <events/Notes.pb.h>


#include "../Typedef.h"
#include "Assignment.h"
#include "Sequence.h"
#include "Locking.h"


namespace fr::musician::sequencer {


using signal_track_type = Sequence<
  sequencer_interval,
  fr::musician::data_definition::SignalTrack>;


using pattern_type = Sequence<
  sequencer_note_event_interval,
  fr::musician::data_definition::Pattern>;


using track_type = Sequence<
  sequencer_key_type,
  fr::musician::data_definition::Track>;


struct event_type {
  typedef typename fr::musician::data_definition::Event attributes_type;
  typedef typename
    fr::musician::data_definition::assignment::SignalTrackAssignment
    assignment_attributes_type;

  typedef typename Assignment<assignment_attributes_type> mapped_type;
  typedef typename std::pair<sequencer_key_type, mapped_type> value_type;


  locking::Mutexes mutexes;
  attributes_type attributes;
  std::map<sequencer_key_type, mapped_type> container;
};


struct signal_type {
  typedef typename fr::musician::data_definition::Signal attributes_type;


  locking::Mutexes mutexes;
  attributes_type attributes;
};


struct song_type {
  typedef typename fr::musician::data_definition::Song attributes_type;

  typedef typename
    fr::musician::data_definition::assignment::TrackAssignment
    assignment_attributes_type;

  typedef typename Assignment<assignment_attributes_type> mapped_type;
  typedef typename std::pair<sequencer_key_type, mapped_type> value_type;


  locking::Mutexes mutexes;
  attributes_type attributes;
  std::map<sequencer_key_type, mapped_type> container;
};


} // end of namespace fr::musician::sequencer


#endif
