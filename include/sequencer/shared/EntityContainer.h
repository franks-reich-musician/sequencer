#ifndef FR_MUSICIAN_SEQUENCER_SHARED_ENTITY_CONTAINER_H
#define FR_MUSICIAN_SEQUENCER_SHARED_ENTITY_CONTAINER_H


#include "Entity.h"
#include "Locking.h"


namespace fr::musician::sequencer {


template <class Entity>
struct EntityContainer {
  EntityContainer():
    mutex(std::make_unique<std::shared_mutex>())
  {}

  typedef typename std::map<sequencer_key_type, Entity> map_type;
  typedef typename map_type::value_type value_type;
  typedef typename Entity mapped_type;

  locking::mutex_type mutex;
  std::map<sequencer_key_type, Entity> container;
};


using signals_type = EntityContainer<signal_type>;
using events_type = EntityContainer<event_type>;
using signal_tracks_type = EntityContainer<signal_track_type>;
using patterns_type = EntityContainer<pattern_type>;
using tracks_type = EntityContainer<track_type>;
using songs_type = EntityContainer<song_type>;


} // end of namespace fr::musician::sequencer


#endif
