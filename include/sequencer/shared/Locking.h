#ifndef FR_MUSICIAN_SEQUENCER_SHARED_LOCKING_H
#define FR_MUSICIAN_SEQUENCER_SHARED_LOCKING_H


#include <memory>
#include <shared_mutex>


namespace fr::musician::sequencer::locking {


using write_lock = std::unique_lock<std::shared_mutex>;
using read_lock = std::shared_lock<std::shared_mutex>;
using mutex_type = std::unique_ptr<std::shared_mutex>;


struct Mutexes {
  Mutexes():
    attributes(std::make_unique<std::shared_mutex>()),
    container(std::make_unique<std::shared_mutex>())
  {}

  mutex_type attributes;
  mutex_type container;
};


} // end of namespace namespace fr::musician::sequencer::locking


#endif
