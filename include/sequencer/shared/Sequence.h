#ifndef FR_MUSICIAN_SEQUENCER_SHARED_SEQUENCE_H
#define FR_MUSICIAN_SEQUENCER_SHARED_SEQUENCE_H


#include <map>


#include "../Typedef.h"
#include "Locking.h"


namespace fr::musician::sequencer {


template <class KeyType, class Attributes>
struct Sequence {
  typedef typename Attributes attributes_type;
  typedef typename KeyType key_type;
  typedef typename std::map<KeyType, sequencer_key_type> container_type;

  locking::Mutexes mutexes;

  Attributes attributes;
  typename container_type container;
};


} // end of namespace fr::musician::sequencer


#endif
