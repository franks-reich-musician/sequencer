#include "sequencer/Sequencer.h"


#include "sequencer/handler/Register.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;


Sequencer::Sequencer():
  _server(REQUEST_QUEUE_NAME)
{
  _registerHandlerFunctions();
}


void Sequencer::_registerHandlerFunctions() {
  auto registry = _server.registry();
  handler::registerHandler(
    registry,
    _songs,
    _tracks,
    _patterns,
    _events,
    _signalTracks,
    _signals);
}
