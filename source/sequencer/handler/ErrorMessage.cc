#include "sequencer/handler/ErrorMessage.h"


#include <sstream>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace std;


void error::prepareDoesNotExistErrorMessage(
  Response &response, string entity, Error_ErrorCode errorCode)
{
  stringstream stringStream;
  stringStream << entity << " does not exist.";
  response.mutable_error()->set_errortext(stringStream.str());
  response.mutable_error()->set_errorcode(errorCode);
}

void error::preparePositionWouldOverlapErrorMessage(
  Response &response, string child, Error_ErrorCode errorCode)
{
  stringstream stringStream;
  stringStream << child << " would overlap.";
  response.mutable_error()->set_errortext(stringStream.str());
  response.mutable_error()->set_errorcode(errorCode);
}

void error::prepareNegativeRangeErrorMessage(
  Response &response, string child)
{
  stringstream stringStream;
  stringStream << "Scheduling of " << child << " was requested with negative "
                                               "length";
  response.mutable_error()->set_errortext(stringStream.str());
  response.mutable_error()->set_errorcode(Error_ErrorCode_NEGATIVE_RANGE_ERROR);
}

void error::preparePositionBaseErrorMessage(
  Response &response,
  string position,
  Error_ErrorCode errorCode)
{
  stringstream stringStream;
  stringStream << "Base of " << position << " position does not match";

  response.mutable_error()->set_errortext(stringStream.str());
  response.mutable_error()->set_errorcode(errorCode);
}
