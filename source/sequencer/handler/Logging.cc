#include "sequencer/handler/Logging.h"


#include <boost/log/trivial.hpp>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace std;


void logging::logCreateHandling(string entity) {
  BOOST_LOG_TRIVIAL(info) << "Creating new " << entity;
}


void logging::logDeleteHandling(string entity, sequencer_key_type id) {
  BOOST_LOG_TRIVIAL(info) << "Deleting " << entity  << " with id " << id;
}


void logging::logDeleteError(string entity, sequencer_key_type id) {
  BOOST_LOG_TRIVIAL(info)
    << "Tried to delete non existing " << entity << " with id " << id;
}


void logging::logGetHandling(string entity, sequencer_key_type id) {
  BOOST_LOG_TRIVIAL(info) << "Reading " << entity << " with id " << id;
}


void logging::logGetError(string entity, sequencer_key_type id) {
  BOOST_LOG_TRIVIAL(info)
    << "Tried to read non existing " << entity << " with id " << id;
}


void logging::logUpdateHandling(string entity, sequencer_key_type id) {
  BOOST_LOG_TRIVIAL(info) << "Updating " << entity << " with id " << id;
}


void logging::logUpdateError(string entity, sequencer_key_type id) {
  BOOST_LOG_TRIVIAL(info)
    << "Tried to update non existing " << entity << " with id " << id;
}

void logging::logAssignHandling(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Assigning " << child << " id: " << childId << " to " << parent
    << " id: " << parentId;
}

void logging::logAssignError(
  string child, string parent, sequencer_key_type childId,
  sequencer_key_type parentId, string errorEntity)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error during assignment of " << child << " id: " << childId << " to "
    << parent << " id: " << parentId << ". " << errorEntity << " does not "
    << " exist.";
}

void logging::logGetAllAssignmentsHandling(
  string child, string parent, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Retrieving all assigned " << child << " from " << parent
    << " id: " << parentId;
}

void logging::logGetAllAssignmentsError(
  string child, string parent, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while retrieving assigned " << child << " from " << parent << "."
    << " " << parent << " id: " << parentId << " does not exist.";
}

void logging::logRemoveAssignmentHandling(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Removing assignment of " << child << " id: " << childId << " from "
    << parent << " id: " << parentId;
}

void logging::logRemoveAssignmentParentError(
  string parent, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while removing assignment. " << parent << " id: " << parentId
    << " does not exist";
}

void logging::logRemoveAssignmentError(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while removing assignment. " << child << " id: " << childId
    << " is not assigned to " << parent << " id: " << parentId;
}

void logging::logUpdateAssignmentHandling(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Updating assignment of " << child << " id: " << childId << " to "
    << parent << " id: " << parentId;
}

void logging::logUpdateAssignmentError(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while updating assignment. Assignment of " << child << " id: "
    << childId << " is not assigned to " << parent << " id: " << parentId;
}

void logging::logUpdateAssignmentParentError(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while updating assignment. " << parent << " id: " << parentId
    << " does not exist.";
}

void logging::logScheduleHandling(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Scheduling " << child << " id: " << childId << " to "
    << parent << " id: " << parentId;
}

void logging::logScheduleError(
  string child, string parent, sequencer_key_type childId,
  sequencer_key_type parentId, string errorEntity)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while scheduling " << child << " id: " << childId
    << " to " << parent << " id: " << parentId << " " << errorEntity
    << " does not exist.";
}

void logging::logOverlappingScheduleError(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while scheduling " << child << " id: " << childId
    << " to " << parent << " id: " << parentId << ". The " << child
    << " would overlap";
}

void logging::logScheduleNegativeRangeError(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while scheduling " << child << " id: " << childId
    << " to " << parent << " id: " << parentId
    << ". The requested schedule range is negative.";
}

void logging::logScheduleBaseMismatchError(
  string child, string parent,
  sequencer_key_type childId, sequencer_key_type parentId,
  string position)
{
  BOOST_LOG_TRIVIAL(info)
    << "Error while scheduling " << child << " id: " << childId
    << " to " << parent << " id: " << parentId
    << ". The base for the " << position << " does not match.";
}
