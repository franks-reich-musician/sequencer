#include "sequencer/handler/Register.h"


#include "sequencer/handler/event/AssignSignalTrackHandler.h"
#include "sequencer/handler/event/CreateEventHandler.h"
#include "sequencer/handler/event/DeleteEventHandler.h"
#include "sequencer/handler/event/GetAssignedSignalTracksHandler.h"
#include "sequencer/handler/event/GetEventHandler.h"
#include "sequencer/handler/event/RemoveSignalTrackHandler.h"
#include "sequencer/handler/event/UpdateEventHandler.h"
#include "sequencer/handler/event/UpdateSignalTrackAssignmentHandler.h"

#include "sequencer/handler/pattern/CreatePatternHandler.h"
#include "sequencer/handler/pattern/DeletePatternHandler.h"
#include "sequencer/handler/pattern/GetPatternHandler.h"
#include "sequencer/handler/pattern/UpdatePatternHandler.h"

#include "sequencer/handler/signal/CreateSignalHandler.h"
#include "sequencer/handler/signal/DeleteSignalHandler.h"
#include "sequencer/handler/signal/GetSignalHandler.h"
#include "sequencer/handler/signal/UpdateSignalHandler.h"

#include "sequencer/handler/signal_track/CreateSignalTrackHandler.h"
#include "sequencer/handler/signal_track/DeleteSignalTrackHandler.h"
#include "sequencer/handler/signal_track/GetSignalTrackHandler.h"
#include "sequencer/handler/signal_track/UpdateSignalTrackHandler.h"

#include "sequencer/handler/song/AssignTrackHandler.h"
#include "sequencer/handler/song/CreateSongHandler.h"
#include "sequencer/handler/song/DeleteSongHandler.h"
#include "sequencer/handler/song/GetAssignedTracksHandler.h"
#include "sequencer/handler/song/GetSongHandler.h"
#include "sequencer/handler/song/RemoveTrackHandler.h"
#include "sequencer/handler/song/UpdateSongHandler.h"
#include "sequencer/handler/song/UpdateTrackAssignmentHandler.h"

#include "sequencer/handler/track/CreateTrackHandler.h"
#include "sequencer/handler/track/DeleteTrackHandler.h"
#include "sequencer/handler/track/GetTrackHandler.h"
#include "sequencer/handler/track/UpdateTrackHandler.h"


using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;


void handler::registerHandler(
  FunctionRegistry &registry,
  songs_type &songs,
  tracks_type &tracks,
  patterns_type &patterns,
  events_type &events,
  signal_tracks_type &signalTracks,
  signals_type &signals)
{
  event::registerAssignSignalTrackHandler(registry, events, signalTracks);
  event::registerCreateEventHandler(registry, events);
  event::registerDeleteEventHandler(registry, events);
  event::registerGetAssignedSignalTracksHandler(registry, events, signalTracks);
  event::registerGetEventHandler(registry, events);
  event::registerRemoveSignalTrackHandler(registry, events);
  event::registerUpdateEventHandler(registry, events);
  event::registerUpdateSignalTrackAssignmentHandler(
    registry, events, signalTracks);

  pattern::registerCreatePatternHandler(registry, patterns);
  pattern::registerDeletePatternHandler(registry, patterns);
  pattern::registerGetPatternHandler(registry, patterns);
  pattern::registerUpdatePatternHandler(registry, patterns);

  signal::registerCreateSignalHandler(registry, signals);
  signal::registerDeleteSignalHandler(registry, signals);
  signal::registerGetSignalHandler(registry, signals);
  signal::registerUpdateSignalHandler(registry, signals);

  signal_track::registerCreateSignalTrackHandler(registry, signalTracks);
  signal_track::registerDeleteSignalTrackHandler(registry, signalTracks);
  signal_track::registerGetSignalTrackHandler(registry, signalTracks);
  signal_track::registerUpdateSignalHandler(registry, signalTracks);

  song::registerAssignTrackHandler(registry, songs, tracks);
  song::registerCreateSongHandler(registry, songs);
  song::registerDeleteSongHandler(registry, songs);
  song::registerGetAssignedTracksHandler(registry, songs, tracks);
  song::registerGetSongHandler(registry, songs);
  song::registerRemoveTrackHandler(registry, songs);
  song::registerUpdateSongHandler(registry, songs);
  song::registerUpdateTrackAssignmentHandler(registry, songs, tracks);

  track::registerCreateTrackHandler(registry, tracks);
  track::registerDeleteTrackHandler(registry, tracks);
  track::registerGetTrackHandler(registry, tracks);
  track::registerUpdateTrackHandler(registry, tracks);
}
