#include "sequencer/handler/ScheduleEntity.h"


using namespace fr::musician::sequencer;
using namespace std;


void handler::checkOverlap(
  const sequencer_note_event_interval &position,
  const typename pattern_type::container_type &container)
{
  constexpr size_t beginIndex = 0;
  constexpr size_t endIndex = 1;
  constexpr size_t octaveIndex = 2;
  constexpr size_t noteIndex = 3;
  const auto beginPosition = get<beginIndex>(position);
  const auto endPosition = get<endIndex>(position);
  if (!container.empty()) {
    auto lowerBound = container.lower_bound(position);

    auto predecessor = lowerBound;
    while (
      (predecessor == container.end()) ||
      (predecessor != container.begin() &&
       get<octaveIndex>(position) != get<octaveIndex>(predecessor->first) &&
       get<noteIndex>(position) != get<noteIndex>(predecessor->first)))
    {
      predecessor--;
    }

    const auto endPredecessor = std::get<endIndex>(predecessor->first);
    if (
      get<octaveIndex>(predecessor->first) == get<octaveIndex>(position) &&
      get<noteIndex>(predecessor->first) == get<noteIndex>(position) &&
      endPredecessor > beginPosition)
    {
      throw overlapping_schedule_error("The predecessor would overlap.");
    }

    auto successor = lowerBound;
    while (
      successor != container.end() &&
      get<octaveIndex>(position) != get<octaveIndex>(successor->first) &&
      get<noteIndex>(position) != get<noteIndex>(successor->first))
    {
      successor--;
    }

    if (successor != container.end()) {
      const auto beginSuccessor = get<beginIndex>(successor->first);
      if (
        get<octaveIndex>(lowerBound->first) == get<octaveIndex>(position) &&
        get<noteIndex>(lowerBound->first) == get<noteIndex>(position) &&
        beginSuccessor < endPosition)
      {
        throw overlapping_schedule_error("The successor would overlap.");
      }
    }
  }
}


void handler::checkOverlap(
  const sequencer_interval &position,
  const typename signal_track_type::container_type &container)
{
  constexpr size_t beginIndex = 0;
  constexpr size_t endIndex = 1;
  const auto beginPosition = get<beginIndex>(position);
  const auto endPosition = get<endIndex>(position);
  if (!container.empty()) {
    auto lowerBound = container.lower_bound(position);

    auto predecessor = lowerBound;
    if (predecessor != container.begin()) {
      predecessor--;
    }

    const auto endPredecessor = std::get<endIndex>(predecessor->first);
    if (endPredecessor > beginPosition) {
      throw overlapping_schedule_error("The predecessor would overlap.");
    }

    auto successor = lowerBound;
    if (successor != container.end()) {
      const auto beginSuccessor = get<beginIndex>(successor->first);
      if (beginSuccessor < endPosition) {
        throw overlapping_schedule_error("The successor would overlap.");
      }
    }
  }
}
