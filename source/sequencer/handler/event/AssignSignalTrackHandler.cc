#include "sequencer/handler/event/AssignSignalTrackHandler.h"


#include <Request.pb.h>
#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void event::registerAssignSignalTrackHandler(
  FunctionRegistry &registry,
  events_type &events,
  signal_tracks_type &signalTracks)
{
  registry.addFunction(Request::DataCase::kAssignSignalTrack, [
    &events,
    &signalTracks,
    handler = CreateAssignment<events_type, signal_tracks_type>(
      events, signalTracks)](
        const Request &request, Response &response) mutable
  {
    const auto eventId = request.assignsignaltrack().eventid();
    const auto signalTrackId = request.assignsignaltrack().signaltrackid();
    logging::logAssignHandling("signal track", "event", signalTrackId, eventId);
    try {
      const auto assignment = request.assignsignaltrack().assignment();
      auto &assignedSignalTrack =
        *response.mutable_assignsignaltrack()->mutable_assignedsignaltrack();
      auto &child = *assignedSignalTrack.mutable_child();
      auto &parent = *assignedSignalTrack.mutable_parent();
      auto &responseAssignment = *assignedSignalTrack.mutable_attributes();
      handler(
        signalTrackId, eventId, assignment, child, parent, responseAssignment);
    } catch (child_range_error &error) {
      logging::logAssignError(
        "signal track", "event", signalTrackId, eventId, "signal track");
      error::prepareDoesNotExistErrorMessage(
        response, "Signal Track", Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
    } catch (parent_range_error &error) {
      logging::logAssignError(
        "signal track", "event", signalTrackId, eventId, "event");
      error::prepareDoesNotExistErrorMessage(
        response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
    }
  });
}
