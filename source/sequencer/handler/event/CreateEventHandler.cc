#include "sequencer/handler/event/CreateEventHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void event::registerCreateEventHandler(
  FunctionRegistry &registry,
  events_type &events)
{
  registry.addFunction(Request::DataCase::kCreateEvent, [
    &events,
    handler = CreateEntity<events_type>(events)](
      const Request &request, Response &response) mutable
  {
    logging::logCreateHandling("event");
    handler(
      request.createevent().event(),
      *response.mutable_createevent()->mutable_event());
  });
}
