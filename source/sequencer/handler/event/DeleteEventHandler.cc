#include "sequencer/handler/event/DeleteEventHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void event::registerDeleteEventHandler(
  FunctionRegistry &registry,
  events_type &events)
{
  registry.addFunction(Request::DataCase::kDeleteEvent, [
    &events,
    handler = DeleteEntity<events_type>(events)](
      const Request &request, Response &response) mutable
  {
    const auto eventId = request.deleteevent().eventid();
    logging::logDeleteHandling("event", eventId);
    try {
      handler(eventId);
      response.mutable_deleteevent();
    } catch (std::range_error &error) {
      logging::logDeleteError("event", eventId);
      error::prepareDoesNotExistErrorMessage(
        response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
    }
  });
}
