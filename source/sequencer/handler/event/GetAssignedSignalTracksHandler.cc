#include "sequencer/handler/event/GetAssignedSignalTracksHandler.h"


#include <Request.pb.h>
#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"
#include "sequencer/handler/GetAllAssignmentIds.h"
#include "sequencer/handler/GetAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


void event::registerGetAssignedSignalTracksHandler(
  FunctionRegistry &registry,
  events_type &events,
  signal_tracks_type &signalTracks)
{
  registry.addFunction(Request::DataCase::kGetAssignedSignalTracks, [
    &events,
    &signalTracks,
    getHandler = GetEntity<events_type>(events),
    getIdsHandler = GetAllAssignmentIds<events_type>(events),
    getAssignmentHandler = GetAssignment<events_type, signal_tracks_type>(
      events, signalTracks)](const Request &request, Response &response) mutable
  {
    const auto eventId = request.getassignedsignaltracks().eventid();
    logging::logGetAllAssignmentsHandling("signal track", "event", eventId);
    try {
      vector<sequencer_key_type> ids;
      getIdsHandler(eventId, ids);
      Event event;
      getHandler(eventId, event);
      for (const auto id: ids) {
        AssignedSignalTrack &assignedSignalTrack =
          *response.mutable_getassignedsignaltracks()->add_tracks();
        getAssignmentHandler(
          id,
          eventId,
          *assignedSignalTrack.mutable_child(),
          *assignedSignalTrack.mutable_attributes());
        *assignedSignalTrack.mutable_parent() = event;
      }
    } catch (parent_range_error &error) {
      logging::logGetAllAssignmentsError("signal track", "event", eventId);
      error::prepareDoesNotExistErrorMessage(
        response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
    }
  });
}
