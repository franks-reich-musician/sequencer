#include "sequencer/handler/event/GetEventHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void event::registerGetEventHandler(
  FunctionRegistry &registry,
  events_type &events)
{
  registry.addFunction(Request::DataCase::kGetEvent, [
    &events,
    handler = GetEntity<events_type>(events)](
      const Request &request, Response &response) mutable
  {
    const auto eventId = request.getevent().eventid();
    logging::logGetHandling("event", eventId);
    try {
      handler(eventId, *response.mutable_getevent()->mutable_event());
    } catch (std::range_error &error) {
      logging::logGetError("event", eventId);
      error::prepareDoesNotExistErrorMessage(
        response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
    }
  });
}
