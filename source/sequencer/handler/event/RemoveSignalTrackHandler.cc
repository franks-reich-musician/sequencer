#include "sequencer/handler/event/RemoveSignalTrackHandler.h"


#include <Request.pb.h>
#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


void event::registerRemoveSignalTrackHandler(
  FunctionRegistry &registry,
  events_type &events)
{
  registry.addFunction(Request::DataCase::kRemoveSignalTrack, [
    &events,
    handler = DeleteAssignment<events_type>(events)](
      const Request &request, Response &response) mutable
  {
    const auto eventId = request.removesignaltrack().eventid();
    const auto signalTrackId = request.removesignaltrack().signaltrackid();
    logging::logRemoveAssignmentHandling(
      "signal track", "event", signalTrackId, eventId);
    try {
      handler(signalTrackId, eventId);
      response.mutable_removesignaltrack();
    } catch (parent_range_error &error) {
      logging::logRemoveAssignmentParentError("event", eventId);
      error::prepareDoesNotExistErrorMessage(
        response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
    } catch (assignment_range_error &error) {
      logging::logRemoveAssignmentError(
        "signal track", "event", signalTrackId, eventId);
      error::prepareDoesNotExistErrorMessage(
        response, "Signal Track Assignment",
        Error_ErrorCode_SIGNAL_TRACK_NOT_ASSIGNED_TO_EVENT);
    }
  });
}
