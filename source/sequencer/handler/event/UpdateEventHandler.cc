#include "sequencer/handler/event/UpdateEventHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/UpdateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void event::registerUpdateEventHandler(
  FunctionRegistry &registry,
  events_type &events)
{
  registry.addFunction(Request::DataCase::kUpdateEvent, [
    &events,
    handler = UpdateEntity<events_type>(events)](
      const Request &request, Response &response) mutable
  {
    const auto eventId = request.updateevent().event().id();
    try {
      logging::logUpdateHandling("event", eventId);
      handler(
        request.updateevent().event(),
        *response.mutable_updateevent()->mutable_event());
    } catch (std::range_error &error) {
      logging::logUpdateError("event", eventId);
      error::prepareDoesNotExistErrorMessage(
        response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
    }
  });
}
