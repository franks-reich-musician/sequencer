#include "sequencer/handler/event/UpdateSignalTrackAssignmentHandler.h"


#include <Request.pb.h>
#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"
#include "sequencer/handler/UpdateAssignment.h"
#include "sequencer/handler/GetAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void event::registerUpdateSignalTrackAssignmentHandler(
  FunctionRegistry &registry, events_type &events,
  signal_tracks_type &signalTracks)
{
  registry.addFunction(Request::DataCase::kUpdateAssignedSignalTrack, [
    &events,
    getHandler = GetEntity<events_type>(events),
    getAssignmentHandler = GetAssignment<events_type, signal_tracks_type>(
      events, signalTracks),
    handler = UpdateAssignment<events_type>(events)](
      const Request &request, Response &response) mutable
    {
      const auto eventId = request.updateassignedsignaltrack().eventid();
      const auto signalTrackId =
        request.updateassignedsignaltrack().signaltrackid();
      logging::logUpdateAssignmentHandling(
        "signal track", "event", signalTrackId, eventId);
      try {
        handler(
          signalTrackId,
          eventId,
          request.updateassignedsignaltrack().assignment());
        auto &assignedSignalTrack =
          *response.mutable_updateassignedsignaltrack()
            ->mutable_assignedsignaltrack();
        getHandler(
          eventId,
          *assignedSignalTrack.mutable_parent());
        getAssignmentHandler(
          signalTrackId,
          eventId,
          *assignedSignalTrack.mutable_child(),
          *assignedSignalTrack.mutable_attributes());
      } catch (parent_range_error &error) {
        logging::logUpdateAssignmentParentError(
          "signal track", "event", signalTrackId, eventId);
        error::prepareDoesNotExistErrorMessage(
          response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
      } catch (assignment_range_error &error) {
        logging::logUpdateAssignmentError(
          "signal track", "event", signalTrackId, eventId);
        error::prepareDoesNotExistErrorMessage(
          response, "Signal Track Assignment",
          Error_ErrorCode_SIGNAL_TRACK_NOT_ASSIGNED_TO_EVENT);
      }
    });
}
