#include "sequencer/handler/pattern/CreatePatternHandler.h"


#include <atomic>


#include <boost/log/trivial.hpp>


#include <Response.pb.h>


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::pattern;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void pattern::registerCreatePatternHandler(
  FunctionRegistry &registry,
  patterns_type &patterns)
{
  registry.addFunction(
    Request::DataCase::kCreatePattern, [
      &patterns,
      handler = CreateEntity<patterns_type>(patterns)](
      const Request &request, Response &response) mutable
    {
      logging::logCreateHandling("pattern");
      handler(
        request.createpattern().pattern(),
        *response.mutable_createpattern()->mutable_pattern());
    });
}
