#include "sequencer/handler/pattern/DeletePatternHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::pattern;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void pattern::registerDeletePatternHandler(
  FunctionRegistry &registry, patterns_type &patterns)
{
  registry.addFunction(
    Request::DataCase::kDeletePattern, [
      &patterns,
      handler = DeleteEntity<patterns_type>(patterns)](
        const Request &request, Response &response) mutable
    {
      const auto patternId = request.deletepattern().patternid();
      logging::logDeleteHandling("pattern", patternId);
      try {
        handler(patternId);
        response.mutable_deletepattern();
      } catch (std::range_error &error) {
        logging::logDeleteError("pattern", patternId);
        error::prepareDoesNotExistErrorMessage(
          response, "Pattern", Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
      }
    });
}
