#include "sequencer/handler/pattern/GetPatternHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::pattern;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void pattern::registerGetPatternHandler(
  FunctionRegistry &registry,
  patterns_type &patterns)
{
  registry.addFunction(
    Request::DataCase::kGetPattern, [
      &patterns,
      handler = GetEntity<patterns_type>(patterns)](
      const Request &request, Response &response) mutable
    {
      const auto patternId = request.getpattern().patternid();
      logging::logGetHandling("pattern", patternId);
      try {
        handler(
          request.getpattern().patternid(),
          *response.mutable_getpattern()->mutable_pattern());
      } catch (std::range_error &error) {
        logging::logGetError("pattern", patternId);
        error::prepareDoesNotExistErrorMessage(
          response, "Pattern", Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
      }
    });
}
