#include "sequencer/handler/pattern/ScheduleEventHandler.h"


#include <Request.pb.h>
#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/ScheduleEntity.h"
#include "sequencer/handler/GetEntity.h"
#include "sequencer/music/Time.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::time;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


void pattern::registerScheduleEventHandler(
  FunctionRegistry &registry,
  patterns_type &patterns,
  events_type &events)
{
  registry.addFunction(
    Request::DataCase::kScheduleEvent, [
      &patterns,
      &events,
      getEvent = GetEntity<events_type>(events),
      getPattern = GetEntity<patterns_type>(patterns),
      handler = ScheduleEntity<patterns_type, events_type>(patterns, events)](
      const Request &request, Response &response) mutable
    {
      const auto patternId = request.scheduleevent().patternid();
      const auto eventId = request.scheduleevent().eventid();
      const auto positionRequest = request.scheduleevent().position();
      logging::logScheduleHandling("event", "pattern", eventId, patternId);
      try {
        const auto position = make_tuple(
          convertMusicTime(positionRequest.begin()),
          convertMusicTime(positionRequest.end()),
          positionRequest.octave(),
          positionRequest.note());
        auto &scheduledEvent =
          *response.mutable_scheduleevent()->mutable_scheduledevent();
        getPattern(patternId, *scheduledEvent.mutable_pattern());

        if (
          scheduledEvent.pattern().basedivision() !=
          positionRequest.begin().denominators(1))
        {
          throw begin_base_mismatch_error(
            "Base of begin position does not match pattern");
        }

        if (
          scheduledEvent.pattern().basedivision() !=
          positionRequest.end().denominators(1))
        {
          throw end_base_mismatch_error(
            "Base of end position does not match pattern");
        }

        handler(eventId, patternId, position);
        getEvent(eventId, *scheduledEvent.mutable_event());
        *scheduledEvent.mutable_position()->mutable_begin() =
          positionRequest.begin();
        *scheduledEvent.mutable_position()->mutable_end() =
          positionRequest.end();
      } catch (child_range_error &error) {
        logging::logScheduleError(
          "event", "pattern", eventId, patternId, "event");
        error::prepareDoesNotExistErrorMessage(
          response, "Event", Error_ErrorCode_EVENT_DOES_NOT_EXIST);
      } catch (range_error &error) {
        logging::logScheduleError(
          "event", "pattern", eventId, patternId, "pattern");
        error::prepareDoesNotExistErrorMessage(
          response, "Pattern", Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
      } catch (overlapping_schedule_error &error) {
        logging::logOverlappingScheduleError(
          "event", "pattern", eventId, patternId);
        error::preparePositionWouldOverlapErrorMessage(
          response, "Event", Error_ErrorCode_POSITION_OVERLAP_ERROR);
      } catch (negative_length_error &error) {
        logging::logScheduleNegativeRangeError(
          "event", "pattern", eventId, patternId);
        error::prepareNegativeRangeErrorMessage(response, "event");
      } catch (begin_base_mismatch_error &error) {
        logging::logScheduleBaseMismatchError(
          "event", "pattern", eventId, patternId, "begin");
        error::preparePositionBaseErrorMessage(
          response, "begin", Error_ErrorCode_BEGIN_POSITION_BASE_ERROR);
      } catch (end_base_mismatch_error &error) {
        logging::logScheduleBaseMismatchError(
          "event", "pattern", eventId, patternId, "end");
        error::preparePositionBaseErrorMessage(
          response, "begin", Error_ErrorCode_END_POSITION_BASE_ERROR);
      }
    });
}
