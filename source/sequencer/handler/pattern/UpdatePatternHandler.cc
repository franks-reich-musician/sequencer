#include "sequencer/handler/pattern/UpdatePatternHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/UpdateEntity.h"


using namespace fr::musician::sequencer::pattern;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::data_definition;
using namespace fr::musician::sequencer;


void pattern::registerUpdatePatternHandler(
  FunctionRegistry &registry, patterns_type &patterns)
{
  registry.addFunction(
    Request::DataCase::kUpdatePattern, [
      &patterns,
      handler = UpdateEntity<patterns_type>(patterns)](
        const Request &request, Response &response) mutable
    {
      const auto patternId = request.updatepattern().pattern().id();
      logging::logUpdateHandling("pattern", patternId);
      try {
        handler(
          request.updatepattern().pattern(),
          *response.mutable_updatepattern()->mutable_pattern());
      } catch (std::range_error &error) {
        logging::logUpdateError("pattern", patternId);
        error::prepareDoesNotExistErrorMessage(
          response, "Pattern", Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
      }
    });
}
