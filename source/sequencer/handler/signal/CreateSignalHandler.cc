#include "sequencer/handler/signal/CreateSignalHandler.h"


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void signal::registerCreateSignalHandler(
  FunctionRegistry &registry, signals_type &signals)
{
  registry.addFunction(
    Request::DataCase::kCreateSignal, [
      &signals,
      handler = CreateEntity<signals_type>(signals)](
        const Request &request, Response &response) mutable
    {
      logging::logCreateHandling("signal");
      handler(
        request.createsignal().signal(),
        *response.mutable_createsignal()->mutable_signal());
    });
}
