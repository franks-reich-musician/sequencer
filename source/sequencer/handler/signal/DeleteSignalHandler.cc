#include "sequencer/handler/signal/DeleteSignalHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void signal::registerDeleteSignalHandler(
  FunctionRegistry &registry, signals_type &signals)
{
  registry.addFunction(
    Request::DataCase::kDeleteSignal, [
      &signals,
      handler = DeleteEntity<signals_type>(signals)](
      const Request &request, Response &response) mutable
    {
      const auto signalId = request.deletesignal().signalid();
      logging::logDeleteHandling("signal", signalId);
      try {
        handler(signalId);
        response.mutable_deletesignal();
      } catch (std::range_error &error) {
        logging::logDeleteError("signal", signalId);
        error::prepareDoesNotExistErrorMessage(
          response, "Signal", Error_ErrorCode_SIGNAL_DOES_NOT_EXIST);
      }
    });
}
