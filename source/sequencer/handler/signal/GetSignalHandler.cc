#include "sequencer/handler/signal/GetSignalHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"

using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;


void signal::registerGetSignalHandler(
  FunctionRegistry &registry, signals_type &signals)
{
  registry.addFunction(
    Request::DataCase::kGetSignal, [
      &signals,
      handler = GetEntity<signals_type>(signals)](
      const Request &request, Response &response) mutable
    {
      const auto signalId = request.getsignal().signalid();
      logging::logGetHandling("signal", signalId);
      try {
        handler(signalId, *response.mutable_getsignal()->mutable_signal());
      } catch (std::range_error &error) {
        logging::logGetError("signal", signalId);
        error::prepareDoesNotExistErrorMessage(
          response, "Signal", Error_ErrorCode_SIGNAL_DOES_NOT_EXIST);
      }
    });
}
