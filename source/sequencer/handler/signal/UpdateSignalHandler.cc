#include "sequencer/handler/signal/UpdateSignalHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/UpdateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;


void signal::registerUpdateSignalHandler(
  FunctionRegistry &registry, signals_type &signals)
{
  registry.addFunction(
    Request::DataCase::kUpdateSignal, [
      &signals,
      handler = UpdateEntity<signals_type>(signals)](
      const Request &request, Response &response) mutable
    {
      const auto signalId = request.updatesignal().signal().id();
      logging::logUpdateHandling("signal", signalId);
      try {
        handler.operator()(
          request.updatesignal().signal(),
          *response.mutable_updatesignal()->mutable_signal());
      } catch (std::range_error &error) {
        logging::logUpdateError("signal", signalId);
        error::prepareDoesNotExistErrorMessage(
          response, "Signal", Error_ErrorCode_SIGNAL_DOES_NOT_EXIST);
      }
    });
}
