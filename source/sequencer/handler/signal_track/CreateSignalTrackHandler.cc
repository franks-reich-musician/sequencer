#include "sequencer/handler/signal_track/CreateSignalTrackHandler.h"


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;


void signal_track::registerCreateSignalTrackHandler(
  FunctionRegistry &registry, signal_tracks_type &signalTracks)
{
  registry.addFunction(
    Request::DataCase::kCreateSignalTrack, [
      &signalTracks,
      handler = CreateEntity<signal_tracks_type>(signalTracks)](
      const Request &request, Response &response) mutable
    {
      logging::logCreateHandling("signal track");
      handler(
        request.createsignaltrack().signaltrack(),
        *response.mutable_createsignaltrack()->mutable_signaltrack());
    });
}
