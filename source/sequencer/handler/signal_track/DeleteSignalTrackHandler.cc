#include "sequencer/handler/signal_track/DeleteSignalTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;


void signal_track::registerDeleteSignalTrackHandler(
  FunctionRegistry &registry, signal_tracks_type &signalTracks)
{
  registry.addFunction(
    Request::DataCase::kDeleteSignalTrack, [
      &signalTracks,
      handler = DeleteEntity<signal_tracks_type>(signalTracks)](
      const Request &request, Response &response) mutable
    {
      const auto signalTrackId = request.deletesignaltrack().signaltrackid();
      logging::logDeleteHandling("signal track", signalTrackId);
      try {
        handler(signalTrackId);
        response.mutable_deletesignaltrack();
      } catch (std::range_error &error) {
        logging::logDeleteError("signal track", signalTrackId);
        error::prepareDoesNotExistErrorMessage(
          response,
          "Signal track",
          Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
      }
    });
}
