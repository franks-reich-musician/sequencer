#include "sequencer/handler/signal_track/GetSignalTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;


void signal_track::registerGetSignalTrackHandler(
  FunctionRegistry &registry, signal_tracks_type &signalTracks)
{
  registry.addFunction(
    Request::DataCase::kGetSignalTrack, [
      &signalTracks,
      handler = GetEntity<signal_tracks_type>(signalTracks)](
      const Request &request, Response &response) mutable
    {
      const auto signalTrackId = request.getsignaltrack().signaltrackid();
      logging::logGetHandling("signal track", signalTrackId);
      try {
        handler(
          request.getsignaltrack().signaltrackid(),
          *response.mutable_getsignaltrack()->mutable_signaltrack());
      } catch (std::range_error &error) {
        logging::logGetError("signal track", signalTrackId);
        error::prepareDoesNotExistErrorMessage(
          response,
          "Signal track",
          Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
      }
    });
}
