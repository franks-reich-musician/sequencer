#include "sequencer/handler/signal_track/UpdateSignalTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/UpdateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::ipc_protocol;


void signal_track::registerUpdateSignalHandler(
  FunctionRegistry &registry, signal_tracks_type &signalTracks)
{
  registry.addFunction(
    Request::DataCase::kUpdateSignalTrack, [
      &signalTracks,
      handler = UpdateEntity<signal_tracks_type>(signalTracks)](
      const Request &request, Response &response) mutable
    {
      const auto signalTrackId = request.updatesignaltrack().signaltrack().id();
      logging::logUpdateHandling("signal track", signalTrackId);
      try {
        handler(
          request.updatesignaltrack().signaltrack(),
          *response.mutable_updatesignaltrack()->mutable_signaltrack());
      } catch (std::range_error &error) {
        logging::logUpdateError("signal track", signalTrackId);
        error::prepareDoesNotExistErrorMessage(
          response, "Signal track",
          Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
      }
    });
}
