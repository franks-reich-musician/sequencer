#include "sequencer/handler/song/AssignTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void song::registerAssignTrackHandler(
  FunctionRegistry &registry,
  songs_type &songs,
  tracks_type &tracks)
{
  registry.addFunction(Request::DataCase::kAssignTrack, [
    &songs,
    &tracks,
    handler = CreateAssignment<songs_type, tracks_type>(songs, tracks)
    ](const Request &request, Response &response) mutable
  {
    const auto songId = request.assigntrack().songid();
    const auto trackId = request.assigntrack().trackid();
    logging::logAssignHandling("track", "song", trackId, songId);
    try {
      const auto assignment = request.assigntrack().assignment();
      auto &song =
        *response.mutable_assigntrack()->
          mutable_assignedtrack()->mutable_parent();
      auto &track =
        *response.mutable_assigntrack()->
          mutable_assignedtrack()->mutable_child();
      auto &responseAssignment =
        *response.mutable_assigntrack()->
          mutable_assignedtrack()->mutable_attributes();
      handler(trackId, songId, assignment, track, song, responseAssignment);
    } catch (parent_range_error &error) {
      logging::logAssignError("track", "song", trackId, songId, "song");
      error::prepareDoesNotExistErrorMessage(
        response, "Signal Track", Error_ErrorCode_SONG_DOES_NOT_EXIST);
    } catch (child_range_error &error) {
      logging::logAssignError("track", "song", trackId, songId, "track");
      error::prepareDoesNotExistErrorMessage(
        response, "Signal Track", Error_ErrorCode_TRACK_DOES_NOT_EXIST);
    }
  });
}
