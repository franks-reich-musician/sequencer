#include "sequencer/handler/song/CreateSongHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::song;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void song::registerCreateSongHandler(
  FunctionRegistry &registry,
  songs_type &songs)
{
  registry.addFunction(
    Request::DataCase::kCreateSong, [
      &songs,
    handler = CreateEntity<songs_type>(songs)](
      const Request &request, Response &response) mutable
    {
      logging::logCreateHandling("song");
      handler(
        request.createsong().song(),
        *response.mutable_createsong()->mutable_song());
    });
}
