#include "sequencer/handler/song/GetAssignedTracksHandler.h"


#include <Request.pb.h>
#include <Response.pb.h>


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"
#include "sequencer/handler/GetAllAssignmentIds.h"
#include "sequencer/handler/GetAssignment.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace std;


void song::registerGetAssignedTracksHandler(
  FunctionRegistry &registry,
  songs_type &songs,
  tracks_type &tracks)
{
  registry.addFunction(Request::DataCase::kGetAssignedTracks, [
    &songs,
    &tracks,
    getHandler = GetEntity<songs_type>(songs),
    getIdsHandler = GetAllAssignmentIds<songs_type>(songs),
    getAssignmentHandler = GetAssignment<songs_type, tracks_type>(
      songs, tracks)](
        const Request &request, Response &response) mutable
  {
    const auto songId = request.getassignedtracks().songid();
    logging::logGetAllAssignmentsHandling("track", "song", songId);
    try {
      vector<sequencer_key_type> ids;
      getIdsHandler(songId, ids);
      Song song;
      getHandler(songId, song);
      for (const auto id: ids) {
        AssignedTrack &assignedTrack =
          *response.mutable_getassignedtracks()->add_tracks();
        getAssignmentHandler(
          id,
          songId,
          *assignedTrack.mutable_child(),
          *assignedTrack.mutable_attributes());
        *assignedTrack.mutable_parent() = song;
      }
    } catch (parent_range_error &error) {
      logging::logGetAllAssignmentsError("track", "song", songId);
      error::prepareDoesNotExistErrorMessage(
        response, "Song", Error_ErrorCode_SONG_DOES_NOT_EXIST);
    }
  });
}
