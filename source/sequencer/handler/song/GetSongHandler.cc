#include "sequencer/handler/song/GetSongHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"
#include "sequencer/handler/ErrorMessage.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::song;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void song::registerGetSongHandler(
  FunctionRegistry &registry,
  songs_type &songs)
{
  registry.addFunction(
    Request::DataCase::kGetSong, [
      &songs,
      handler = GetEntity<songs_type>(songs)](
      const Request &request, Response &response) mutable
    {
      const auto songId = request.getsong().songid();
      logging::logGetHandling("song", songId);
      try {
        handler(songId, *response.mutable_getsong()->mutable_song());
      } catch (std::range_error &error) {
        logging::logGetError("song", songId);
        error::prepareDoesNotExistErrorMessage(
          response, "Song", Error_ErrorCode_SONG_DOES_NOT_EXIST);
      }
    });
}
