#include "sequencer/handler/song/RemoveTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::song;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void song::registerRemoveTrackHandler(
  FunctionRegistry &registry,
  songs_type &songs)
{
  registry.addFunction(Request::DataCase::kRemoveTrack, [
    &songs,
    handler = DeleteAssignment<songs_type>(songs)](
      const Request &request, Response &response) mutable
  {
    const auto songId = request.removetrack().songid();
    const auto trackId = request.removetrack().trackid();
    logging::logRemoveAssignmentHandling("track", "song", trackId, songId);
    try {
      handler(trackId, songId);
      response.mutable_removetrack();
    } catch (parent_range_error &error) {
      logging::logRemoveAssignmentParentError("song", songId);
      error::prepareDoesNotExistErrorMessage(
        response, "Song", Error_ErrorCode_SONG_DOES_NOT_EXIST);
    } catch (assignment_range_error &error) {
      logging::logRemoveAssignmentError("track", "song", trackId, songId);
      error::prepareDoesNotExistErrorMessage(
        response,
        "Track Assignment",
        Error_ErrorCode_TRACK_NOT_ASSIGNED_TO_SONG);
    }
  });
}
