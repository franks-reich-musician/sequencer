#include "sequencer/handler/song/UpdateSongHandler.h"


#include <Response.pb.h>


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/UpdateEntity.h"
#include "sequencer/handler/ErrorMessage.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::sequencer::song;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace std;


void song::registerUpdateSongHandler(
  FunctionRegistry &registry,
  songs_type &songs)
{
  registry.addFunction(
    Request::DataCase::kUpdateSong, [
      &songs,
      handler = UpdateEntity<songs_type>(songs)](
      const Request &request, Response &response) mutable
    {
      const auto songId = request.updatesong().song().id();
      logging::logUpdateHandling("song", songId);
      try {
        handler(
          request.updatesong().song(),
          *response.mutable_updatesong()->mutable_song());
      } catch (std::range_error &error) {
        logging::logUpdateError("song", songId);
        error::prepareDoesNotExistErrorMessage(
          response, "Song", Error_ErrorCode_SONG_DOES_NOT_EXIST);
      }
    });
}
