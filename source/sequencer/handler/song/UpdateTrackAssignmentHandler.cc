#include "sequencer/handler/song/UpdateTrackAssignmentHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"
#include "sequencer/handler/UpdateAssignment.h"
#include "sequencer/handler/GetAssignment.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void song::registerUpdateTrackAssignmentHandler(
  FunctionRegistry &registry,
  songs_type &songs,
  tracks_type &tracks)
{
  registry.addFunction(Request::DataCase::kUpdateAssignedTrack, [
    &songs,
    &tracks,
    getHandler = GetEntity<songs_type>(songs),
    getAssignmentHandler = GetAssignment<songs_type, tracks_type>(
      songs, tracks),
    handler = UpdateAssignment<songs_type>(songs)](
      const Request &request, Response &response) mutable
  {
    const auto songId = request.updateassignedtrack().songid();
    const auto trackId = request.updateassignedtrack().trackid();
    logging::logUpdateAssignmentHandling("track", "song", trackId, songId);
    try {
      handler(trackId, songId, request.updateassignedtrack().assignment());
      auto &assignedTrack =
        *response.mutable_updateassignedtrack()->mutable_assignedtrack();
      getHandler(songId, *assignedTrack.mutable_parent());
      getAssignmentHandler(
        trackId,
        songId,
        *assignedTrack.mutable_child(),
        *assignedTrack.mutable_attributes());
    } catch (parent_range_error &error) {
      logging::logUpdateAssignmentParentError("track", "song", trackId, songId);
      error::prepareDoesNotExistErrorMessage(
        response, "Song", Error_ErrorCode_SONG_DOES_NOT_EXIST);
    } catch (assignment_range_error &error) {
      logging::logUpdateAssignmentError("track", "song", trackId, songId);
      error::prepareDoesNotExistErrorMessage(
        response, "Track Assignment",
        Error_ErrorCode_TRACK_NOT_ASSIGNED_TO_SONG);
    }
  });
}
