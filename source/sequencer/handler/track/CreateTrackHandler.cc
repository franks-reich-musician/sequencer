#include "sequencer/handler/track/CreateTrackHandler.h"


#include "sequencer/handler/Logging.h"
#include "sequencer/handler/CreateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::sequencer::track;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::data_definition;


void track::registerCreateTrackHandler(
  FunctionRegistry &registry, tracks_type &tracks)
{
  registry.addFunction(
    Request::DataCase::kCreateTrack, [
      &tracks,
      handler = CreateEntity<tracks_type>(tracks)](
        const Request &request, Response &response) mutable
    {
      logging::logCreateHandling("track");
      handler(
        request.createtrack().track(),
        *response.mutable_createtrack()->mutable_track());
    });
}
