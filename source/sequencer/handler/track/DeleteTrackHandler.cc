#include "sequencer/handler/track/DeleteTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/DeleteEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::sequencer::track;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::data_definition;


void track::registerDeleteTrackHandler(
  FunctionRegistry &registry, tracks_type &tracks)
{
  registry.addFunction(
    Request::DataCase::kDeleteTrack, [
      &tracks,
      handler = DeleteEntity<tracks_type>(tracks)](
      const Request &request, Response &response) mutable
    {
      const auto trackId = request.deletetrack().trackid();
      logging::logDeleteHandling("track", trackId);
      try {
        handler(trackId);
        response.mutable_deletetrack();
      } catch (std::range_error &error) {
        logging::logDeleteError("track", trackId);
        error::prepareDoesNotExistErrorMessage(
          response, "Track", Error_ErrorCode_TRACK_DOES_NOT_EXIST);
      }
    });
}
