#include "sequencer/handler/track/GetTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/GetEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::sequencer::track;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::data_definition;


void track::registerGetTrackHandler(
  FunctionRegistry &registry, tracks_type &tracks)
{
  registry.addFunction(
    Request::DataCase::kGetTrack, [
      &tracks,
      handler = GetEntity<tracks_type>(tracks)](
      const Request &request, Response &response) mutable
    {
      const auto trackId = request.gettrack().trackid();
      logging::logGetHandling("track", trackId);
      try {
        handler(trackId, *response.mutable_gettrack()->mutable_track());
      } catch (std::range_error &error) {
        logging::logGetError("track", trackId);
        error::prepareDoesNotExistErrorMessage(
          response, "Track", Error_ErrorCode_TRACK_DOES_NOT_EXIST);
      }
    });
}
