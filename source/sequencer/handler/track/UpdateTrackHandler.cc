#include "sequencer/handler/track/UpdateTrackHandler.h"


#include "sequencer/handler/ErrorMessage.h"
#include "sequencer/handler/Logging.h"
#include "sequencer/handler/UpdateEntity.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::sequencer::track;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::data_definition;


void track::registerUpdateTrackHandler(
  FunctionRegistry &registry, tracks_type &tracks)
{
  registry.addFunction(
    Request::DataCase::kUpdateTrack, [
      &tracks,
      handler = UpdateEntity<tracks_type>(tracks)](
      const Request &request, Response &response) mutable
    {
      const auto trackId = request.updatetrack().track().id();
      logging::logUpdateHandling("track", trackId);
      try {
        handler(
          request.updatetrack().track(),
          *response.mutable_updatetrack()->mutable_track());
      } catch (std::range_error &error) {
        logging::logUpdateError("track", trackId);
        error::prepareDoesNotExistErrorMessage(
          response, "Track", Error_ErrorCode_TRACK_DOES_NOT_EXIST);
      }
    });
}
