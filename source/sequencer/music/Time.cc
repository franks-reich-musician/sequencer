#include "sequencer/music/Time.h"


#include "sequencer/music/Exceptions.h"


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::time;
using namespace fr::musician::data_definition::time;


void time::checkMusicTime(MusicTime time) {
  if (time.denominators_size() != time.divisions_size()) {
    throw denominator_and_division_count_mismatch(
      "Denominator and division counts do not match up");
  }

  const sequencer_time_type base = time.denominators(1);
  auto expected = base;
  for (size_t i = 2; i < time.denominators_size(); i++) {
    expected = expected * base;
    if (expected != time.denominators(i)) {
      throw denominator_error("Denominators are not formed correctly.");
    }
  }

  const auto resolution = time.resolution();
  while (expected < resolution) {
    expected = expected * base;
  }

  if (expected != resolution) {
    throw resolution_error("Resolution is not a power of base.");
  }
}


sequencer_time_type time::convertMusicTime(MusicTime time) {
  const auto resolution = time.resolution();
  sequencer_time_type result = 0;
  for (size_t i = 0; i < time.denominators_size(); i++) {
    result += time.divisions(i) * (resolution / time.denominators(i));
  }
  result += time.remainder();
  return result;
}


MusicTime time::convertInternalTime(
  sequencer_time_type time,
  sequencer_time_type base,
  sequencer_time_type numberOfDenominators,
  sequencer_time_type resolution)
{
  MusicTime musicTime;
  musicTime.set_resolution(resolution);
  sequencer_key_type remainder = time;
  sequencer_key_type denominator = 1;
  for (size_t i = 0; i < numberOfDenominators; i++) {
    const sequencer_key_type currentDivisor = (resolution / denominator);
    sequencer_key_type result = remainder / currentDivisor;
    remainder -= result * currentDivisor;
    musicTime.add_divisions(result);
    musicTime.add_denominators(denominator);
    denominator *= base;
  }
  musicTime.set_remainder(remainder);

  return musicTime;
}
