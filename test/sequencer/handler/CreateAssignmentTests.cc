#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <assignment/TrackAssignment.pb.h>
#include <Song.pb.h>
#include <Track.pb.h>
#include <sequencer/handler/CreateAssignment.h>
#include <sequencer/handler/CreateEntity.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::assignment;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreateAssignmentTests)


class CreateAssignmentTestsFixture {
  public:
    CreateAssignmentTestsFixture():
      createSong(songs),
      createTrack(tracks),
      assign(songs, tracks)
    {
      Song song;
      Song songResponse;
      createSong(song, songResponse);
      songId = songResponse.id();

      Track track;
      Track trackResponse;
      createTrack(track, trackResponse);
      trackId = trackResponse.id();
    }

    sequencer_key_type songId;
    sequencer_key_type trackId;
    tracks_type tracks;
    songs_type songs;
    CreateEntity<tracks_type> createTrack;
    CreateEntity<songs_type> createSong;
    CreateAssignment<songs_type, tracks_type> assign;
};


BOOST_FIXTURE_TEST_CASE(
  assign_handler_assignment_test,
  CreateAssignmentTestsFixture,
  *description(
    "The assign handler should add the key of the child to the track attribute"
    "of the parent if both parent and child exist."))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  TrackAssignment responseAssignment;
  Track track;
  Song song;
  assign(trackId, songId, assignment, track, song, responseAssignment);
  BOOST_REQUIRE(
    songs.container.find(songId)->second.container.find(trackId) !=
    songs.container.find(songId)->second.container.end());

  const auto songIter = songs.container.find(songId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    song, songIter->second.attributes));

  const auto trackIter = tracks.container.find(trackId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    track, trackIter->second.attributes));

  const auto assignmentIter = songIter->second.container.find(trackId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    responseAssignment, assignmentIter->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  assign_handler_assignment_attributes,
  CreateAssignmentTestsFixture,
  *description(
    "The assign handler should add the assignment attributes."))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  assignment.set_name("Test");
  TrackAssignment responseAssignment;
  Track track;
  Song song;
  assign(trackId, songId, assignment, track, song, responseAssignment);
  BOOST_REQUIRE(
    songs.container.at(
      songId).container.find(trackId)->second.attributes.name().compare("Test") == 0);
}


BOOST_FIXTURE_TEST_CASE(
  assign_handler_negative_child_test,
  CreateAssignmentTestsFixture,
  *description(
    "The assign handler should throw a child range error if the child does not"
    "exist."))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  TrackAssignment responseAssignment;
  Track track;
  Song song;
  BOOST_REQUIRE_EXCEPTION(
    assign(42, songId, assignment, track, song, responseAssignment),
    child_range_error,
    [](const child_range_error &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  assign_handler_negative_parent_test,
  CreateAssignmentTestsFixture,
  *description(
    "The assign handler should throw a parent range error if the parent does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  TrackAssignment responseAssignment;
  Track track;
  Song song;
  BOOST_REQUIRE_EXCEPTION(
    assign(trackId, 42, assignment, track, song, responseAssignment),
    parent_range_error,
    [](const parent_range_error &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
