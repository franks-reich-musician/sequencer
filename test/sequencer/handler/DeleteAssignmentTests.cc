#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <assignment/TrackAssignment.pb.h>
#include <Song.pb.h>
#include <Track.pb.h>
#include <sequencer/handler/CreateAssignment.h>
#include <sequencer/handler/DeleteAssignment.h>
#include <sequencer/handler/CreateEntity.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::assignment;
using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(DeleteAssignmentTests)


class DeleteAssignmentTestsFixture {
  public:
    DeleteAssignmentTestsFixture():
      createSong(songs),
      createTrack(tracks),
      createAssignment(songs, tracks),
      deleteAssignment(songs)
    {
      Song song;
      Song songResponse;
      createSong(song, songResponse);
      songId = songResponse.id();

      Track track;
      Track trackResponse;
      createTrack(track, trackResponse);
      trackId = trackResponse.id();

      TrackAssignment assignment;
      assignment.set_name("Test");
      TrackAssignment responseAssignment;
      createAssignment(
        trackId, songId, assignment, track, song, responseAssignment);
    }


    sequencer_key_type songId;
    sequencer_key_type trackId;
    tracks_type tracks;
    songs_type songs;
    CreateEntity<tracks_type> createTrack;
    CreateEntity<songs_type> createSong;
    CreateAssignment<songs_type, tracks_type> createAssignment;
    DeleteAssignment<songs_type> deleteAssignment;
};


BOOST_FIXTURE_TEST_CASE(
  delete_assignment_test,
  DeleteAssignmentTestsFixture,
  *description(
    "The delete handler should remove the assignment from the parents"
    "container."))
{
  boost::debug::detect_memory_leaks(false);
  BOOST_REQUIRE_EQUAL(
    songs.container.at(songId).container.size(), 1);

  deleteAssignment(trackId, songId);
  BOOST_REQUIRE_EQUAL(
    songs.container.at(songId).container.size(), 0);
}


BOOST_FIXTURE_TEST_CASE(
  delete_assignment_parent_negative_test,
  DeleteAssignmentTestsFixture,
  *description(
    "The delete handler should throw a parent_range_error when the parent"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  BOOST_REQUIRE_EXCEPTION(
    deleteAssignment(trackId, 42),
    parent_range_error,
    [](const parent_range_error &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  delete_assignment_negative_test,
  DeleteAssignmentTestsFixture,
  *description(
    "The delete handler should throw a assignment range error when the"
    "assignment does not exist in the parent's container."))
{
  boost::debug::detect_memory_leaks(false);
  BOOST_REQUIRE_EXCEPTION(
    deleteAssignment(42, songId),
    assignment_range_error,
    [](const assignment_range_error &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
