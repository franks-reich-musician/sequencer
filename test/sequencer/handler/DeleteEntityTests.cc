#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <sequencer/handler/CreateEntity.h>
#include <sequencer/handler/DeleteEntity.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(DeleteEntityTests)


class DeleteEntityTestsFixture {
  public:
    DeleteEntityTestsFixture():
      patterns(),
      createHandler(patterns),
      deleteHandler(patterns)
    {}


    patterns_type patterns;
    CreateEntity<patterns_type> createHandler;
    DeleteEntity<patterns_type> deleteHandler;
};


BOOST_FIXTURE_TEST_CASE(
  delete_entity_test,
  DeleteEntityTestsFixture,
  *description(
    "The delete entity handler should delete the entity from the container."))
{
  boost::debug::detect_memory_leaks(false);

  Pattern input;
  input.set_name("Test");
  Pattern output;
  createHandler(input, output);
  BOOST_REQUIRE_EQUAL(patterns.container.size(), 1);

  deleteHandler(output.id());
  BOOST_REQUIRE_EQUAL(patterns.container.size(), 0);
}


BOOST_FIXTURE_TEST_CASE(
  delete_entity_negative_test,
  DeleteEntityTestsFixture,
  *description(
    "The delete entity handler should throw an exception if the entity with"
    "the id does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  BOOST_REQUIRE_EXCEPTION(
    deleteHandler(42), range_error, [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
