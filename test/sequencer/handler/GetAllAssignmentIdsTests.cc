#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Song.pb.h>
#include <Track.pb.h>
#include <assignment/TrackAssignment.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <sequencer/handler/CreateAssignment.h>
#include <sequencer/handler/GetAllAssignmentIds.h>
#include <sequencer/handler/CreateEntity.h>
#include <AssignedTrack.pb.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::assignment;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(GetAllAssignmentIdsTests)


class GetAllAssignmentIdsTestsFixture {
  public:
    GetAllAssignmentIdsTestsFixture():
      createTrack(tracks),
      createSong(songs),
      createAssignment(songs, tracks),
      getAllAssignments(songs)
    {
      Song song;
      Song songResponse;
      createSong(song, songResponse);
      songId = songResponse.id();

      for (size_t i = 0; i < 5; i++) {
        Track track;
        track.set_name("Test Track 1");
        Track trackResponse;
        createTrack(track, trackResponse);
        const auto trackId = trackResponse.id();
        trackIds.push_back(trackId);

        TrackAssignment assignment;
        assignment.set_name("Assignment");
        TrackAssignment responseAssignment;
        createAssignment(
          trackId, songId, assignment, track, song, responseAssignment);
      }
    }


    sequencer_key_type songId;
    vector<sequencer_key_type> trackIds;
    tracks_type tracks;
    songs_type songs;
    CreateEntity<tracks_type> createTrack;
    CreateEntity<songs_type> createSong;
    CreateAssignment<songs_type, tracks_type> createAssignment;
    GetAllAssignmentIds<songs_type> getAllAssignments;
};


BOOST_FIXTURE_TEST_CASE(
  get_all_assignment_ids_handler_test,
  GetAllAssignmentIdsTestsFixture,
  *description(
    "The get all assignments handler should return a vector of all"
    "assignment ids."))
{
  boost::debug::detect_memory_leaks(false);
  vector<sequencer_key_type> result;
  getAllAssignments(songId, result);

  BOOST_REQUIRE_EQUAL_COLLECTIONS(
    result.cbegin(), result.cend(), trackIds.cbegin(), trackIds.cend());
}


BOOST_FIXTURE_TEST_CASE(
  get_all_assignment_ids_parent_negative_test,
  GetAllAssignmentIdsTestsFixture,
  *description(
    "The get all assignments handler should throw a parent range error if"
    "the parent does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  vector<sequencer_key_type> result;
  BOOST_REQUIRE_EXCEPTION(
    getAllAssignments(42, result),
    parent_range_error,
    [](const parent_range_error &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
