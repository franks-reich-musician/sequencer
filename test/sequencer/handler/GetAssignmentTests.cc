#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Song.pb.h>
#include <Track.pb.h>
#include <assignment/TrackAssignment.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <sequencer/handler/CreateAssignment.h>
#include <sequencer/handler/GetAssignment.h>
#include <sequencer/handler/CreateEntity.h>
#include <AssignedTrack.pb.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::assignment;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(GetAssignmentTests)


class GetAssignmentTestsFixture {
  public:
    GetAssignmentTestsFixture():
      createTrack(tracks),
      createSong(songs),
      createAssignment(songs, tracks),
      getAssignment(songs, tracks)
    {
      Song song;
      Song songResponse;
      createSong(song, songResponse);
      songId = songResponse.id();

      Track track;
      track.set_name("Test Track 1");
      Track trackResponse;
      createTrack(track, trackResponse);
      trackId = trackResponse.id();

      TrackAssignment assignment;
      TrackAssignment responseAssignment;
      assignment.set_name("Assignment");
      createAssignment(
        trackId, songId, assignment, track, song, responseAssignment);
    }

    sequencer_key_type songId;
    sequencer_key_type trackId;
    tracks_type tracks;
    songs_type songs;
    CreateEntity<tracks_type> createTrack;
    CreateEntity<songs_type> createSong;
    CreateAssignment<songs_type, tracks_type> createAssignment;
    GetAssignment<songs_type, tracks_type> getAssignment;
};


BOOST_FIXTURE_TEST_CASE(
  get_handler_test,
  GetAssignmentTestsFixture,
  *description(
    "The get assignments handler should return the assigned child and the"
    "assignment attributes."))
{
  boost::debug::detect_memory_leaks(false);
  Track track;
  TrackAssignment assignedTrack;
  getAssignment(trackId, songId, track, assignedTrack);

  const auto expectedSong = songs.container.find(songId);
  const auto expectedTrack = tracks.container.find(trackId);
  const auto expectedAssignment = expectedSong->second.container.find(trackId);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    track, expectedTrack->second.attributes));

  BOOST_REQUIRE(MessageDifferencer::Equals(
    assignedTrack, expectedAssignment->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  get_handler_parent_negative_test,
  GetAssignmentTestsFixture,
  *description(
    "The get handler should throw a parent range error if the parent does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Track track;
  TrackAssignment assignedTrack;
  BOOST_REQUIRE_EXCEPTION(
    getAssignment(trackId, 42, track, assignedTrack),
    parent_range_error,
    [](const parent_range_error &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  get_handler_child_negative_test,
  GetAssignmentTestsFixture,
  *description(
    "The get handler should throw a child range error if the child does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Track track;
  TrackAssignment assignedTrack;
  BOOST_REQUIRE_EXCEPTION(
    getAssignment(42, songId, track, assignedTrack),
    child_range_error,
    [](const child_range_error &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  get_handler_assignment_negative_test,
  GetAssignmentTestsFixture,
  *description(
    "The get handler should throw a assignment range error if the assignment"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Track track;
  track.set_name("Test Track 1");
  Track trackResponse;
  createTrack(track, trackResponse);
  const auto unassignedTrackId = trackResponse.id();

  TrackAssignment assignedTrack;
  BOOST_REQUIRE_EXCEPTION(
    getAssignment(unassignedTrackId, songId, track, assignedTrack),
    assignment_range_error,
    [](const assignment_range_error &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
