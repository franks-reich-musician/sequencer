#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <stdexcept>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <sequencer/handler/GetEntity.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace google::protobuf::util;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(GetEntityTests)


class GetEntityTestsFixture {
  public:
    GetEntityTestsFixture():
      patterns(),
      handler(patterns)
    {
      testPattern.set_id(4);
      testPattern.set_name("Test pattern");
      pattern_type pattern = pattern_type();
      pattern.attributes = testPattern;
      patterns.container.insert(
        patterns_type::value_type(4, move(pattern)));
    }

    ~GetEntityTestsFixture() {}

    Pattern testPattern;
    patterns_type patterns;
    GetEntity<patterns_type> handler;
};


BOOST_FIXTURE_TEST_CASE(
  get_entity_test,
  GetEntityTestsFixture,
  *description("The get entity handler should return an entity if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Pattern result;
  handler(4, result);
  BOOST_REQUIRE(MessageDifferencer::Equals(result, testPattern));
}


BOOST_FIXTURE_TEST_CASE(
  get_entity_negative_test,
  GetEntityTestsFixture,
  *description(
    "The get entity handler should throw a range_error if the id does not"
    "exist."))
{
  boost::debug::detect_memory_leaks(false);
  Pattern result;
  BOOST_REQUIRE_EXCEPTION(
    handler(7, result), range_error, [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()

