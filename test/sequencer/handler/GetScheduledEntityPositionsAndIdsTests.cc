#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <Pattern.pb.h>
#include <Event.pb.h>
#include <events/Notes.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/ScheduleEntity.h>
#include <sequencer/handler/CreateEntity.h>
#include <sequencer/handler/GetScheduledEntityPositionsAndIdsTests.h>
#include <sequencer/shared/EntityContainer.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::events;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(GetScheduledEntityPositionsAndIdsTests)


class GetScheduledEntityPositionsAndIdsTestsFixture {
  public:
    GetScheduledEntityPositionsAndIdsTestsFixture():
      createEvent(events),
      createPattern(patterns),
      scheduleEntity(patterns, events),
      getScheduledEntities(patterns)
    {
      Pattern patternRequest;
      Pattern patternResponse;
      createPattern(patternRequest, patternResponse);
      patternId = patternResponse.id();

      for (size_t i = 0; i < 10; i++) {
        Event eventRequest;
        Event eventResponse;
        createEvent(eventRequest, eventResponse);
        eventIds.push_back(eventResponse.id());
      }

      schedule = {
        make_pair(make_tuple(1, 2, 0, Notes::C), eventIds.at(0)),
        make_pair(make_tuple(2, 4, 0, Notes::D), eventIds.at(1)),
        make_pair(make_tuple(2, 4, 0, Notes::E), eventIds.at(2)),
        make_pair(make_tuple(3, 6, 0, Notes::F), eventIds.at(3)),
        make_pair(make_tuple(4, 5, 0, Notes::C), eventIds.at(4)),
        make_pair(make_tuple(4, 5, 0, Notes::E), eventIds.at(5)),
        make_pair(make_tuple(4, 6, 0, Notes::D), eventIds.at(6)),
        make_pair(make_tuple(4, 6, 0, Notes::F), eventIds.at(7)),
        make_pair(make_tuple(5, 6, 0, Notes::C), eventIds.at(8)),
        make_pair(make_tuple(8, 10, 0, Notes::C), eventIds.at(9))
      };

      for (const auto &event: schedule) {
        scheduleEntity(event.second, patternId, event.first);
      }
    }


    sequencer_key_type patternId;
    vector<sequencer_key_type> eventIds;
    patterns_type patterns;
    events_type events;
    CreateEntity<patterns_type> createPattern;
    CreateEntity<events_type> createEvent;
    ScheduleEntity<patterns_type, events_type> scheduleEntity;
    GetScheduledEntityPositionsAndIds<patterns_type> getScheduledEntities;
    vector<pair<sequencer_note_event_interval, sequencer_key_type>> schedule;
};


BOOST_FIXTURE_TEST_CASE(
  get_scheduled_entity_positions_and_ids_test,
  GetScheduledEntityPositionsAndIdsTestsFixture,
  *description(
    "The get scheduled entity positions and ids handler should return the"
    "positions and ids of all entities scheduled between begin and end."))
{
  boost::debug::detect_memory_leaks(false);

  GetScheduledEntityPositionsAndIds<patterns_type>::results_type results;
  getScheduledEntities(3, 5, patternId, results);
  size_t index = 1;
  BOOST_REQUIRE_EQUAL(results.size(), 7);
  for (const auto actual: results) {
    const auto expected = schedule.at(index);
    BOOST_REQUIRE_EQUAL(get<0>(actual.first), get<0>(expected.first));
    BOOST_REQUIRE_EQUAL(get<1>(actual.first), get<1>(expected.first));
    BOOST_REQUIRE_EQUAL(get<2>(actual.first), get<2>(expected.first));
    BOOST_REQUIRE_EQUAL(get<3>(actual.first), get<3>(expected.first));
    BOOST_REQUIRE_EQUAL(actual.second, expected.second);
    index++;
  }
}


BOOST_FIXTURE_TEST_CASE(
  get_scheduled_entity_positions_and_ids_negative_length_negative_test,
  GetScheduledEntityPositionsAndIdsTestsFixture,
  *description(
    "The get scheduled entity positions and ids handler should throw"
    "a negative length error if the begin of the range is after the"
    "end of the range"))
{
  boost::debug::detect_memory_leaks(false);

  GetScheduledEntityPositionsAndIds<patterns_type>::results_type results;
  BOOST_REQUIRE_EXCEPTION(
    getScheduledEntities(6, 2, patternId, results),
    negative_length_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  get_scheduled_entity_positions_and_ids_parent_negative_test,
  GetScheduledEntityPositionsAndIdsTestsFixture,
  *description(
    "The get scheduled entity positions and ids handler should throw"
    "a parent range error if the parent does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  GetScheduledEntityPositionsAndIds<patterns_type>::results_type results;
  BOOST_REQUIRE_EXCEPTION(
    getScheduledEntities(1, 2, 42, results),
    parent_range_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
