#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <Pattern.pb.h>
#include <Event.pb.h>
#include <events/Notes.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/ScheduleEntity.h>
#include <sequencer/handler/CreateEntity.h>
#include <sequencer/handler/RemoveScheduledEntity.h>
#include <sequencer/shared/EntityContainer.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::events;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(RemoveScheduledEntityTests)


class RemoveScheduledEntityTestsFixture {
  public:
    RemoveScheduledEntityTestsFixture():
      createPattern(patterns),
      createEvent(events),
      scheduleEntity(patterns, events),
      removeScheduledEntity(patterns)
    {
      Pattern patternRequest;
      Pattern patternResponse;
      createPattern(patternRequest, patternResponse);
      patternId = patternResponse.id();

      for (size_t i = 0; i < 10; i++) {
        Event eventRequest;
        Event eventResponse;
        createEvent(eventRequest, eventResponse);
        eventIds.push_back(eventResponse.id());
      }

      schedule = {
        make_pair(make_tuple(0, 4, 0, Notes::D), eventIds.at(0)),
        make_pair(make_tuple(5, 6, 0, Notes::D), eventIds.at(1)),
        make_pair(make_tuple(6, 7, 0, Notes::D), eventIds.at(2)),
        make_pair(make_tuple(7, 9, 0, Notes::D), eventIds.at(3)),
        make_pair(make_tuple(10, 14, 0, Notes::D), eventIds.at(4))
      };

      for (const auto &event: schedule) {
        scheduleEntity(event.second, patternId, event.first);
      }
    }


    sequencer_key_type patternId;
    vector<sequencer_key_type> eventIds;
    patterns_type patterns;
    events_type events;
    CreateEntity<patterns_type> createPattern;
    CreateEntity<events_type> createEvent;
    ScheduleEntity<patterns_type, events_type> scheduleEntity;
    RemoveScheduledEntity<patterns_type> removeScheduledEntity;
    vector<pair<sequencer_note_event_interval, sequencer_key_type>> schedule;
};


BOOST_FIXTURE_TEST_CASE(
  remove_scheduled_entity_handler_test,
  RemoveScheduledEntityTestsFixture,
  *description(
    "The remove scheduled entity handler should remove the entity if it is"
    "scheduled at the position."))
{
  boost::debug::detect_memory_leaks(false);

  auto eventToDelete = schedule.at(2);
  schedule.erase(schedule.begin() + 2);
  removeScheduledEntity(eventToDelete.second, patternId, eventToDelete.first);

  const auto pattern = patterns.container.find(patternId);
  BOOST_REQUIRE_EQUAL(pattern->second.container.size(), 4);
  size_t index = 0;
  for (const auto &scheduledEvent: pattern->second.container) {
    BOOST_REQUIRE_EQUAL(
      get<0>(schedule.at(index).first),
      get<0>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      get<1>(schedule.at(index).first),
      get<1>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      get<2>(schedule.at(index).first),
      get<2>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      get<3>(schedule.at(index).first),
      get<3>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      scheduledEvent.second,
      schedule.at(index).second);
    index++;
  }
}


BOOST_FIXTURE_TEST_CASE(
  remove_scheduled_entity_handler_parent_negative_test,
  RemoveScheduledEntityTestsFixture,
  *description(
    "The remove scheduled entity handler should throw a parent range error"
    "if the parent does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  const auto event = schedule.at(3);
  BOOST_REQUIRE_EXCEPTION(
    removeScheduledEntity(event.second, 42, event.first),
    parent_range_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  remove_scheduled_entity_handler_schedule_event_id_negative_test,
  RemoveScheduledEntityTestsFixture,
  *description(
    "The remove scheduled entity handler should throw a child not scheduled"
    "error if the child is not scheduled with the parent."))
{
  boost::debug::detect_memory_leaks(false);

  const auto event = schedule.at(3);
  BOOST_REQUIRE_EXCEPTION(
    removeScheduledEntity(42, patternId, event.first),
    child_not_scheduled_at_position,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  remove_scheduled_entity_handler_schedule_position_negative_test,
  RemoveScheduledEntityTestsFixture,
  *description(
    "The remove scheduled entity handler should throw a child not scheduled"
    "error if the child is not scheduled at the given position."))
{
  boost::debug::detect_memory_leaks(false);

  const auto event = schedule.at(3);
  BOOST_REQUIRE_EXCEPTION(
    removeScheduledEntity(
      event.second, patternId, make_tuple(0, 5, 2, Notes::E)),
    child_not_scheduled_at_position,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  remove_scheduled_entity_handler_negative_length_negative_test,
  RemoveScheduledEntityTestsFixture,
  *description(
    "The remove scheduled entity handler should throw a negative length error"
    "if the position has a negative length."))
{
  boost::debug::detect_memory_leaks(false);

  auto position = make_tuple(5, 3, 0, Notes::E_SHARP);
  const auto event = schedule.at(3);
  BOOST_REQUIRE_EXCEPTION(
    removeScheduledEntity(event.second, patternId, position),
    negative_length_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
