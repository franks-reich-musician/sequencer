#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <Pattern.pb.h>
#include <Event.pb.h>
#include <events/Notes.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/ScheduleEntity.h>
#include <sequencer/handler/CreateEntity.h>
#include <sequencer/shared/EntityContainer.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::events;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(ScheduleEntityTests)


class ScheduleEntityTestsFixture {
  public:
    ScheduleEntityTestsFixture():
      createEvent(events),
      createPattern(patterns),
      scheduleEntity(patterns, events)
    {
      Pattern requestPattern;
      Pattern responsePattern;
      createPattern(requestPattern, responsePattern);
      patternId = responsePattern.id();

      for (size_t i = 0; i < 10; i ++) {
        Event requestEvent;
        Event responseEvent;
        createEvent(requestEvent, responseEvent);
        eventIds.push_back(responseEvent.id());
      }
    }


    sequencer_key_type patternId;
    vector<sequencer_key_type> eventIds;
    patterns_type patterns;
    events_type events;
    CreateEntity<patterns_type> createPattern;
    CreateEntity<events_type> createEvent;
    ScheduleEntity<patterns_type, events_type> scheduleEntity;
};


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should schedule the child entity with"
    "the parent entity if both exist."))
{
  boost::debug::detect_memory_leaks(false);

  auto position = make_tuple(5, 10, 0, Notes::E_SHARP);
  scheduleEntity(eventIds.at(0), patternId, position);

  const auto pattern = patterns.container.find(patternId);
  const auto scheduledEventId = pattern->second.container.find(position);
  BOOST_REQUIRE_EQUAL(scheduledEventId->second, eventIds.at(0));
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_parent_negative_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should throw a parent range error if the"
    "parent does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  auto position = make_tuple(5, 10, 0, Notes::E_SHARP);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(eventIds.at(0), 42, position),
    parent_range_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_child_negative_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should throw a child range error if the"
    "child does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  auto position = make_tuple(5, 10, 0, Notes::E_SHARP);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(42, patternId, position),
    child_range_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_successor_overlap_negative_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should throw a schedule overlap error if"
    "the succeeding entity would overlap with the inserted one if it does not"
    "differ in additional parts of the key"))
{
  boost::debug::detect_memory_leaks(false);

  auto positionOne = make_tuple(8, 10, 0,Notes::E_SHARP);
  auto positionTwo = make_tuple(7, 9, 0, Notes::E_SHARP);
  scheduleEntity(eventIds.at(0), patternId, positionOne);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(eventIds.at(0), patternId, positionTwo),
    overlapping_schedule_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_predecessor_overlap_negative_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should throw a schedule overlap error if"
    "the preceding entity would overlap with the inserted one if it does"
    "not differ in additional parts of the key."))
{
  boost::debug::detect_memory_leaks(false);

  auto positionOne = make_tuple(6, 8, 0, Notes::E_SHARP);
  auto positionTwo = make_tuple(7, 9, 0, Notes::E_SHARP);
  scheduleEntity(eventIds.at(0), patternId, positionOne);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(eventIds.at(0), patternId, positionTwo),
    overlapping_schedule_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  schedule_overlapping_entity_with_different_key_remainder,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should schedule the even even though it"
    "overlaps with another entity if it differs in other parts of the key."))
{
  boost::debug::detect_memory_leaks(false);

  auto positionOne = make_tuple(6, 8, 0, Notes::E_SHARP);
  auto positionTwo = make_tuple(7, 9, 0, Notes::D);
  auto positionThree = make_tuple(7, 9, 1, Notes::D);
  scheduleEntity(eventIds.at(0), patternId, positionOne);
  scheduleEntity(eventIds.at(1), patternId, positionTwo);
  scheduleEntity(eventIds.at(2), patternId, positionThree);

  const auto pattern = patterns.container.find(patternId);
  const auto scheduledEventOne = pattern->second.container.find(positionOne);
  const auto scheduledEventTwo = pattern->second.container.find(positionTwo);
  const auto scheduledEventThree = pattern->second.container.find(positionThree);
  BOOST_REQUIRE_EQUAL(scheduledEventOne->second, eventIds.at(0));
  BOOST_REQUIRE_EQUAL(scheduledEventTwo->second, eventIds.at(1));
  BOOST_REQUIRE_EQUAL(scheduledEventThree->second, eventIds.at(2));
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_negative_length_negative_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should throw a negative length error if"
    "the entity is scheduled with a negative length"))
{
  boost::debug::detect_memory_leaks(false);

  auto position = make_tuple(5, 3, 0, Notes::E_SHARP);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(eventIds.at(0), patternId, position),
    negative_length_error,
    [](const exception &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_multi_events_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should schedule multiple entities with"
    "different key remainder correctly."))
{
  boost::debug::detect_memory_leaks(false);

  vector<pair<sequencer_note_event_interval, sequencer_key_type>> testEvents {
    make_pair(make_tuple(2, 4, 0, Notes::C), eventIds.at(0)),
    make_pair(make_tuple(2, 4, 0, Notes::D), eventIds.at(1)),
    make_pair(make_tuple(2, 4, 0, Notes::E), eventIds.at(2)),
    make_pair(make_tuple(3, 6, 0, Notes::F), eventIds.at(3)),
    make_pair(make_tuple(4, 5, 0, Notes::C), eventIds.at(4)),
    make_pair(make_tuple(4, 5, 0, Notes::E), eventIds.at(5)),
    make_pair(make_tuple(4, 6, 0, Notes::D), eventIds.at(6)),
    make_pair(make_tuple(4, 6, 0, Notes::F), eventIds.at(7)),
    make_pair(make_tuple(5, 6, 0, Notes::C), eventIds.at(8)),
    make_pair(make_tuple(8, 10, 0, Notes::C), eventIds.at(9))
  };

  for (const auto &testEvent: testEvents) {
    scheduleEntity(testEvent.second, patternId, testEvent.first);
  }

  const auto pattern = patterns.container.find(patternId);
  size_t index = 0;
  for (const auto &scheduledEvent: pattern->second.container) {
    BOOST_REQUIRE_EQUAL(
      get<0>(testEvents.at(index).first),
      get<0>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      get<1>(testEvents.at(index).first),
      get<1>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      get<2>(testEvents.at(index).first),
      get<2>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      get<3>(testEvents.at(index).first),
      get<3>(scheduledEvent.first));
    BOOST_REQUIRE_EQUAL(
      scheduledEvent.second,
      eventIds.at(index));
    index++;
  }
}


BOOST_FIXTURE_TEST_CASE(
  schedule_entity_multi_events_overlap_negative_test,
  ScheduleEntityTestsFixture,
  *description(
    "The schedule entity handler should throw an overlap error if one of the"
    "already scheduled entities overlap even though there are multiple"
    "non-overlapping entities scheduled beforehand."))
{
  boost::debug::detect_memory_leaks(false);

  vector<pair<sequencer_note_event_interval, sequencer_key_type>> testEvents {
    make_pair(make_tuple(2, 4, 0, Notes::C), eventIds.at(0)),
    make_pair(make_tuple(2, 4, 0, Notes::D), eventIds.at(1)),
    make_pair(make_tuple(2, 4, 0, Notes::E), eventIds.at(2)),
    make_pair(make_tuple(3, 6, 0, Notes::F), eventIds.at(3)),
    make_pair(make_tuple(4, 5, 0, Notes::C), eventIds.at(4)),
    make_pair(make_tuple(4, 5, 0, Notes::E), eventIds.at(5)),
    make_pair(make_tuple(4, 6, 0, Notes::D), eventIds.at(6)),
    make_pair(make_tuple(4, 6, 0, Notes::F), eventIds.at(7)),
    make_pair(make_tuple(5, 7, 0, Notes::C), eventIds.at(8)),
    make_pair(make_tuple(8, 10, 0, Notes::C), eventIds.at(9))
  };

  for (const auto &testEvent: testEvents) {
    scheduleEntity(testEvent.second, patternId, testEvent.first);
  }

  auto position = make_tuple(6, 7, 0, Notes::C);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(eventIds.at(0), patternId, position),
    overlapping_schedule_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
