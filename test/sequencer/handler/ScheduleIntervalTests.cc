#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <SignalTrack.pb.h>
#include <Signal.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/ScheduleEntity.h>
#include <sequencer/handler/CreateEntity.h>
#include <sequencer/shared/EntityContainer.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(ScheduleIntervalTests)


class ScheduleIntervalTestsFixture {
  public:
    ScheduleIntervalTestsFixture():
      createSignals(signals),
      createSignalTracks(signalTracks),
      scheduleEntity(signalTracks, signals)
    {
      SignalTrack requestSignalTrack;
      SignalTrack responseSignalTrack;
      createSignalTracks(requestSignalTrack, responseSignalTrack);
      signalTrackId = responseSignalTrack.id();

      for (size_t i = 0; i < 10; i++) {
        Signal signalRequest;
        Signal signalResponse;
        createSignals(signalRequest, signalResponse);
        signalIds.push_back(signalResponse.id());
      }
    }


    sequencer_key_type signalTrackId;
    vector<sequencer_key_type> signalIds;
    signal_tracks_type signalTracks;
    signals_type signals;
    CreateEntity<signal_tracks_type> createSignalTracks;
    CreateEntity<signals_type> createSignals;
    ScheduleEntity<signal_tracks_type, signals_type> scheduleEntity;
};


BOOST_FIXTURE_TEST_CASE(
  schedule_interval_multiple_entities_test,
  ScheduleIntervalTestsFixture,
  *description(
    "The schedule handler should schedule multiple entities with only"
    "intervals as long as they do not overlap"))
{
  boost::debug::detect_memory_leaks(false);

  vector<pair<sequencer_interval, sequencer_key_type>> testEntities {
    make_pair(make_tuple(0, 3), signalIds.at(0)),
    make_pair(make_tuple(4, 6), signalIds.at(1)),
    make_pair(make_tuple(6, 7), signalIds.at(2)),
    make_pair(make_tuple(8, 9), signalIds.at(3)),
    make_pair(make_tuple(9, 11), signalIds.at(4)),
    make_pair(make_tuple(12, 13), signalIds.at(5)),
    make_pair(make_tuple(13, 15), signalIds.at(6)),
    make_pair(make_tuple(16, 18), signalIds.at(7)),
    make_pair(make_tuple(20, 22), signalIds.at(8)),
    make_pair(make_tuple(22, 23), signalIds.at(9))
  };

  for (const auto &testEntity: testEntities) {
    scheduleEntity(testEntity.second, signalTrackId, testEntity.first);
  }

  const auto signalTrack = signalTracks.container.find(signalTrackId);
  BOOST_REQUIRE_EQUAL(signalTrack->second.container.size(), 10);
  size_t index = 0;
  for (const auto &scheduledSignal: signalTrack->second.container) {
    BOOST_REQUIRE_EQUAL(
      get<0>(testEntities.at(index).first),
      get<0>(scheduledSignal.first));
    BOOST_REQUIRE_EQUAL(
      get<1>(testEntities.at(index).first),
      get<1>(scheduledSignal.first));
    BOOST_REQUIRE_EQUAL(
      scheduledSignal.second,
      signalIds.at(index));
    index++;
  }
}


BOOST_FIXTURE_TEST_CASE(
  schedule_interval_multiple_entities_overlap_negative_test,
  ScheduleIntervalTestsFixture,
  *description(
    "The schedule handler should throw an overlap error if the interval of"
    "the new entity would overlap."))
{
  boost::debug::detect_memory_leaks(false);

  vector<pair<sequencer_interval, sequencer_key_type>> testEntities {
    make_pair(make_tuple(0, 3), signalIds.at(0)),
    make_pair(make_tuple(4, 6), signalIds.at(1)),
    make_pair(make_tuple(6, 7), signalIds.at(2)),
    make_pair(make_tuple(8, 9), signalIds.at(3)),
    make_pair(make_tuple(9, 11), signalIds.at(4)),
    make_pair(make_tuple(12, 13), signalIds.at(5)),
    make_pair(make_tuple(13, 15), signalIds.at(6)),
    make_pair(make_tuple(16, 18), signalIds.at(7)),
    make_pair(make_tuple(20, 22), signalIds.at(8))
  };

  for (const auto &testEntity: testEntities) {
    scheduleEntity(testEntity.second, signalTrackId, testEntity.first);
  }

  const auto position = make_tuple(14, 16);
  BOOST_REQUIRE_EXCEPTION(
    scheduleEntity(signalIds.at(9), signalTrackId, position),
    overlapping_schedule_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
