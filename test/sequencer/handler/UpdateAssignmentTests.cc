#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <assignment/TrackAssignment.pb.h>
#include <Song.pb.h>
#include <Track.pb.h>
#include <sequencer/handler/CreateAssignment.h>
#include <sequencer/handler/UpdateAssignment.h>
#include <sequencer/handler/CreateEntity.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::assignment;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateAssignmentTests)


class UpdateAssignmentTestsFixture {
  public:
    UpdateAssignmentTestsFixture():
      createSong(songs),
      createTrack(tracks),
      createAssignment(songs, tracks),
      updateAssignment(songs)
    {
      Song song;
      Song songResponse;
      createSong(song, songResponse);
      songId = songResponse.id();

      Track track;
      Track trackResponse;
      createTrack(track, trackResponse);
      trackId = trackResponse.id();

      TrackAssignment assignment;
      TrackAssignment responseAssignment;
      assignment.set_name("Test");
      createAssignment(
        trackId, songId, assignment, track, song, responseAssignment);
    }


    sequencer_key_type songId;
    sequencer_key_type trackId;
    tracks_type tracks;
    songs_type songs;
    CreateEntity<tracks_type> createTrack;
    CreateEntity<songs_type> createSong;
    CreateAssignment<songs_type, tracks_type> createAssignment;
    UpdateAssignment<songs_type> updateAssignment;
};


BOOST_FIXTURE_TEST_CASE(
  update_handler_test,
  UpdateAssignmentTestsFixture,
  *description(
    "The update handler should update the attributes of the assignment."))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  assignment.set_name("Updated name");
  updateAssignment(trackId, songId, assignment);

  const auto storedAssignment =
    songs.container.at(songId).container.at(trackId).attributes;

  BOOST_REQUIRE(MessageDifferencer::Equals(assignment, storedAssignment));
}


BOOST_FIXTURE_TEST_CASE(
  update_handler_parent_negative_test,
  UpdateAssignmentTestsFixture,
  *description(
    "The update handler should throw a parent range error when the parent"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  BOOST_REQUIRE_EXCEPTION(
    updateAssignment(trackId, 42, assignment),
    parent_range_error,
    [](const parent_range_error &ex) { return true; });
}


BOOST_FIXTURE_TEST_CASE(
  update_handler_assignment_negative_test,
  UpdateAssignmentTestsFixture,
  *description(
    "The update handler should throw an assignment range error if the"
    "assignment does not exist"))
{
  boost::debug::detect_memory_leaks(false);
  TrackAssignment assignment;
  BOOST_REQUIRE_EXCEPTION(
    updateAssignment(42, songId, assignment),
    assignment_range_error,
    [](const assignment_range_error &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
