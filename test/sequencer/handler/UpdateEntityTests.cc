#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/Typedef.h>
#include <sequencer/shared/EntityContainer.h>
#include <sequencer/handler/CreateEntity.h>
#include <sequencer/handler/UpdateEntity.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::handler;
using namespace fr::musician::data_definition;
using namespace google::protobuf::util;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(UpdateEntityTests)


class UpdateEntityTestsFixture {
  public:
    UpdateEntityTestsFixture():
      patterns(),
      createHandler(patterns),
      updateHandler(patterns)
    {}

    ~UpdateEntityTestsFixture() {}

    patterns_type patterns;
    CreateEntity<patterns_type> createHandler;
    UpdateEntity<patterns_type> updateHandler;
};


BOOST_FIXTURE_TEST_CASE(
  update_entity_test,
  UpdateEntityTestsFixture,
  *description(
    "The update entity handler should update the entity."))
{
  boost::debug::detect_memory_leaks(false);

  Pattern request;
  request.set_name("Test");
  Pattern response;
  createHandler(request, response);

  request = response;
  request.set_name("Test new");
  updateHandler(request, response);
  BOOST_ASSERT(MessageDifferencer::Equals(request, response));

  const auto pattern = patterns.container.find(response.id());
  BOOST_ASSERT(MessageDifferencer::Equals(request, pattern->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_entity_negative_test,
  UpdateEntityTestsFixture,
  *description(
    "The update handler should throw a range_error if the entity does not"
    "exist."))
{
  boost::debug::detect_memory_leaks(false);
  Pattern request;
  request.set_id(42);
  Pattern response;
  BOOST_REQUIRE_EXCEPTION(
    updateHandler(request, response),
    range_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
