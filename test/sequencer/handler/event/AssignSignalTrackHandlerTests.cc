#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Request.pb.h>
#include <Response.pb.h>


#include <sequencer/handler/event/AssignSignalTrackHandler.h>
#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(AssignSignalTrackHandlerTests)


class AssignSignalTrackHandlerTestsFixture {
  public:
    AssignSignalTrackHandlerTestsFixture() {
      registerAssignSignalTrackHandler(registry, events, signalTracks);
      registerCreateEventHandler(registry, events);
      registerCreateSignalTrackHandler(registry, signalTracks);

      Request request;
      request.mutable_createevent()->mutable_event()->set_name("Test event");
      Response response;
      registry.function(Request::DataCase::kCreateEvent)(request, response);
      eventId = response.createevent().event().id();

      request.mutable_createsignaltrack()->mutable_signaltrack()->set_name("St");
      registry.function(Request::DataCase::kCreateSignalTrack)(
        request, response);
      signalTrackId = response.createsignaltrack().signaltrack().id();
    }


    sequencer_key_type signalTrackId;
    sequencer_key_type eventId;
    FunctionRegistry registry;
    events_type events;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  assign_signal_track_handler_test,
  AssignSignalTrackHandlerTestsFixture,
  *description(
    "The assign signal track handler should assign a signal track to an"
    "event."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_assignsignaltrack()->mutable_assignment()->set_name("Ta");
  request.mutable_assignsignaltrack()->set_signaltrackid(signalTrackId);
  request.mutable_assignsignaltrack()->set_eventid(eventId);
  Response response;
  registry.function(Request::DataCase::kAssignSignalTrack)(
    request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.assignsignaltrack().assignment(),
    response.assignsignaltrack().assignedsignaltrack().attributes()));

  const auto event = events.container.find(eventId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    event->second.attributes,
    response.assignsignaltrack().assignedsignaltrack().parent()));

  const auto signalTrack = signalTracks.container.find(signalTrackId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    signalTrack->second.attributes,
    response.assignsignaltrack().assignedsignaltrack().child()));

  const auto assignment = event->second.container.find(signalTrackId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    assignment->second.attributes,
    response.assignsignaltrack().assignedsignaltrack().attributes()));
}


BOOST_FIXTURE_TEST_CASE(
  assign_signal_track_handler_event_negative_test,
  AssignSignalTrackHandlerTestsFixture,
  *description(
    "The assign signal track handler should return an error if the event"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_assignsignaltrack()->mutable_assignment()->set_name("Ta");
  request.mutable_assignsignaltrack()->set_signaltrackid(signalTrackId);
  request.mutable_assignsignaltrack()->set_eventid(42);
  Response response;
  registry.function(Request::DataCase::kAssignSignalTrack)(
    request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  assign_signal_track_handler_signal_track_negative_test,
  AssignSignalTrackHandlerTestsFixture,
  *description(
    "The assign signal track handler should return an error if the signal"
    "track does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_assignsignaltrack()->mutable_assignment()->set_name("Ta");
  request.mutable_assignsignaltrack()->set_signaltrackid(42);
  request.mutable_assignsignaltrack()->set_eventid(eventId);
  Response response;
  registry.function(Request::DataCase::kAssignSignalTrack)(
    request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
