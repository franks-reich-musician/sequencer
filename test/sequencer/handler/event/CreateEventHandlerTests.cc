#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/event/CreateEventHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreateEventHandlerTests)


class CreateEventHandlerTestsFixture {
  public:
    CreateEventHandlerTestsFixture() {
      event::registerCreateEventHandler(registry, events);
    }


    FunctionRegistry registry;
    events_type events;
};


BOOST_FIXTURE_TEST_CASE(
  create_handler_test,
  CreateEventHandlerTestsFixture,
  *description(
    "The create handler should create the event."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createevent()->mutable_event()->set_name("Test event");
  Response response;
  registry.function(Request::DataCase::kCreateEvent)(request, response);

  BOOST_REQUIRE_EQUAL(events.container.size(), 1);

  request.mutable_createevent()->mutable_event()->set_id(
    response.createevent().event().id());

  BOOST_ASSERT(MessageDifferencer::Equals(
    response.createevent().event(),
    request.createevent().event()));

  const auto event = events.container.find(response.createevent().event().id());
  BOOST_ASSERT(MessageDifferencer::Equals(
    response.createevent().event(),
    event->second.attributes));
}


BOOST_AUTO_TEST_SUITE_END()
