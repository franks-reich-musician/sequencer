#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/event/DeleteEventHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(DeleteEventHandlerTests)


class DeleteEventHandlerTestsFixture {
  public:
    DeleteEventHandlerTestsFixture() {
      event::registerCreateEventHandler(registry, events);
      event::registerDeleteEventHandler(registry, events);
    }


    FunctionRegistry registry;
    events_type events;
};


BOOST_FIXTURE_TEST_CASE(
  delete_handler_test,
  DeleteEventHandlerTestsFixture,
  *description(
    "The delete handler should delete the event"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createevent()->mutable_event()->set_name("Test event");
  Response response;
  registry.function(Request::DataCase::kCreateEvent)(request, response);

  BOOST_REQUIRE_EQUAL(events.container.size(), 1);

  request.mutable_deleteevent()->set_eventid(
    response.createevent().event().id());
  registry.function(Request::DataCase::kDeleteEvent)(request, response);

  BOOST_REQUIRE_EQUAL(events.container.size(), 0);
  BOOST_ASSERT(response.has_deleteevent());
}


BOOST_FIXTURE_TEST_CASE(
  delete_handler_negative_test,
  DeleteEventHandlerTestsFixture,
  *description(
    "The delete handler should return an error if the event does not exist"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_deleteevent()->set_eventid(42);
  Response response;
  registry.function(Request::DataCase::kDeleteEvent)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
