#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <algorithm>
#include <sstream>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/event/GetAssignedSignalTracksHandler.h>
#include <sequencer/handler/event/AssignSignalTrackHandler.h>
#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(GetAssignedSignalTracksHandlerTests)


class GetAssignedSignalTracksHandlerTestsFixture {
  public:
    GetAssignedSignalTracksHandlerTestsFixture() {
      registerAssignSignalTrackHandler(registry, events, signalTracks);
      registerGetAssignedSignalTracksHandler(registry, events, signalTracks);
      registerCreateEventHandler(registry, events);
      registerCreateSignalTrackHandler(registry, signalTracks);

      Request request;
      request.mutable_createevent()->mutable_event()->set_name("Test event");
      Response response;
      registry.function(Request::DataCase::kCreateEvent)(request, response);
      eventId = response.createevent().event().id();

      for (size_t i = 0; i < 5; i++) {
        stringstream ss;
        ss << "Signal Track" << i;
        request.mutable_createsignaltrack()->mutable_signaltrack()->set_name(
          ss.str());
        registry.function(Request::DataCase::kCreateSignalTrack)(
          request, response);
        const auto signalTrackId =
          response.createsignaltrack().signaltrack().id();

        request.mutable_assignsignaltrack()->set_eventid(eventId);
        request.mutable_assignsignaltrack()->set_signaltrackid(signalTrackId);
        ss << " Assignment";
        request.mutable_assignsignaltrack()->mutable_assignment()->set_name(
          ss.str());
        registry.function(Request::DataCase::kAssignSignalTrack)(
          request, response);
        signalTrackIds.push_back(signalTrackId);
      }
    }


    vector<sequencer_key_type> signalTrackIds;
    sequencer_key_type eventId;
    FunctionRegistry registry;
    events_type events;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  get_assigned_signal_tracks_handler_tests,
  GetAssignedSignalTracksHandlerTestsFixture,
  *description(
    "The get assigned signal tracks handler should return all signal tracks"
    "assigned to the event."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_getassignedsignaltracks()->set_eventid(eventId);
  Response response;
  registry.function(Request::DataCase::kGetAssignedSignalTracks)(
    request, response);

  const auto numAssignments = response.getassignedsignaltracks().tracks_size();
  BOOST_REQUIRE_EQUAL(numAssignments, signalTrackIds.size());
  BOOST_REQUIRE_EQUAL(numAssignments, 5);

  const auto event = events.container.find(eventId);
  vector<sequencer_key_type> assignedSignalTrackIds;
  transform(
    event->second.container.cbegin(),
    event->second.container.cend(),
    back_inserter(assignedSignalTrackIds),
    [](const auto &assignedSignalTrack) { return assignedSignalTrack.first; });

  BOOST_REQUIRE_EQUAL(numAssignments, assignedSignalTrackIds.size());

  for (size_t i = 0; i < numAssignments; i++) {
    AssignedSignalTrack assignedSignalTrack;
    *assignedSignalTrack.mutable_parent() = event->second.attributes;
    const auto signalTrackId = assignedSignalTrackIds.at(i);
    const auto signalTrack = signalTracks.container.find(signalTrackId);
    *assignedSignalTrack.mutable_child() = signalTrack->second.attributes;
    const auto assignment = event->second.container.find(signalTrackId);
    *assignedSignalTrack.mutable_attributes() = assignment->second.attributes;
    BOOST_REQUIRE(MessageDifferencer::Equals(
      response.getassignedsignaltracks().tracks(i),
      assignedSignalTrack));
  }
}


BOOST_FIXTURE_TEST_CASE(
  get_assigned_signal_handler_event_negative_tests,
  GetAssignedSignalTracksHandlerTestsFixture,
  *description(
    "The get assigned signal tracks handler should return an error if the"
    "event does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_getassignedsignaltracks()->set_eventid(42);
  Response response;
  registry.function(Request::DataCase::kGetAssignedSignalTracks)(
    request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
