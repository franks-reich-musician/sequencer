#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/event/GetEventHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(GetEventHandlerTests)


class GetEventHandlerTestsFixture {
  public:
    GetEventHandlerTestsFixture() {
      event::registerCreateEventHandler(registry, events);
      event::registerGetEventHandler(registry, events);
    }


    FunctionRegistry registry;
    events_type events;
};


BOOST_FIXTURE_TEST_CASE(
  get_handler_test,
  GetEventHandlerTestsFixture,
  *description(
    "The get handler should return the event"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createevent()->mutable_event()->set_name("Test event");
  Response response;
  registry.function(Request::DataCase::kCreateEvent)(request, response);

  request.mutable_getevent()->set_eventid(response.createevent().event().id());
  Response getResponse;
  registry.function(Request::DataCase::kGetEvent)(request, getResponse);

  BOOST_ASSERT(MessageDifferencer::Equals(
    response.createevent().event(),
    getResponse.getevent().event()));
}


BOOST_FIXTURE_TEST_CASE(
  get_handler_negative_test,
  GetEventHandlerTestsFixture,
  *description(
    "The get handler should return an error if the event does not exist"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_getevent()->set_eventid(42);
  Response response;
  registry.function(Request::DataCase::kGetEvent)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
