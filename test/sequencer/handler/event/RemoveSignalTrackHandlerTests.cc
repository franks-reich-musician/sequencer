#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sstream>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/event/RemoveSignalTrackHandler.h>
#include <sequencer/handler/event/AssignSignalTrackHandler.h>
#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(RemoveSignalTrackHandlerTests)


class RemoveSignalTrackHandlerTestsFixture {
  public:
    RemoveSignalTrackHandlerTestsFixture() {
      registerAssignSignalTrackHandler(registry, events, signalTracks);
      registerRemoveSignalTrackHandler(registry, events);
      registerCreateEventHandler(registry, events);
      registerCreateSignalTrackHandler(registry, signalTracks);

      Request request;
      request.mutable_createevent()->mutable_event()->set_name("Test event");
      Response response;
      registry.function(Request::DataCase::kCreateEvent)(request, response);
      eventId = response.createevent().event().id();

      for (size_t i = 0; i < 5; i++) {
        stringstream ss;
        ss << "Signal Track" << i;
        request.mutable_createsignaltrack()->mutable_signaltrack()->set_name(
          ss.str());
        registry.function(Request::DataCase::kCreateSignalTrack)(
          request, response);
        const auto signalTrackId =
          response.createsignaltrack().signaltrack().id();

        request.mutable_assignsignaltrack()->set_eventid(eventId);
        request.mutable_assignsignaltrack()->set_signaltrackid(signalTrackId);
        ss << " Assignment";
        request.mutable_assignsignaltrack()->mutable_assignment()->set_name(
          ss.str());
        registry.function(Request::DataCase::kAssignSignalTrack)(
          request, response);
        signalTrackIds.push_back(signalTrackId);
      }
    }


    vector<sequencer_key_type> signalTrackIds;
    sequencer_key_type eventId;
    FunctionRegistry registry;
    events_type events;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  remove_signal_track_handler_test,
  RemoveSignalTrackHandlerTestsFixture,
  *description(
    "The remove signal track handler should remove the signal track from the"
    "event."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_removesignaltrack()->set_eventid(eventId);
  request.mutable_removesignaltrack()->set_signaltrackid(signalTrackIds.at(2));
  Response response;
  registry.function(Request::DataCase::kRemoveSignalTrack)(request, response);

  const auto event = events.container.find(eventId);
  BOOST_REQUIRE_EQUAL(event->second.container.size(), 4);
  BOOST_REQUIRE(response.has_removesignaltrack());
}


BOOST_FIXTURE_TEST_CASE(
  remove_signal_track_handler_event_negative_test,
  RemoveSignalTrackHandlerTestsFixture,
  *description(
    "The remove signal track handler should return an error if the event"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_removesignaltrack()->set_eventid(42);
  request.mutable_removesignaltrack()->set_signaltrackid(signalTrackIds.at(0));
  Response response;
  registry.function(Request::DataCase::kRemoveSignalTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  remove_signal_track_handler_assignment_negative_test,
  RemoveSignalTrackHandlerTestsFixture,
  *description(
    "The remove signal track handler should return an error if the signal"
    "track is not assigned to the event."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_removesignaltrack()->set_eventid(eventId);
  request.mutable_removesignaltrack()->set_signaltrackid(42);
  Response response;
  registry.function(Request::DataCase::kRemoveSignalTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SIGNAL_TRACK_NOT_ASSIGNED_TO_EVENT);
}


BOOST_AUTO_TEST_SUITE_END()
