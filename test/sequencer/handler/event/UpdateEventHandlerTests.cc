#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/event/UpdateEventHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateEventHandlerTests)


class UpdateEventHandlerTestsFixture {
  public:
    UpdateEventHandlerTestsFixture() {
      event::registerCreateEventHandler(registry, events);
      event::registerUpdateEventHandler(registry, events);
    }


    FunctionRegistry registry;
    events_type events;
};


BOOST_FIXTURE_TEST_CASE(
  update_handler_test,
  UpdateEventHandlerTestsFixture,
  *description(
    "The update handler should update the event"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createevent()->mutable_event()->set_name("Test event");
  Response response;
  registry.function(Request::DataCase::kCreateEvent)(request, response);

  *request.mutable_updateevent()->mutable_event() =
    response.createevent().event();
  request.mutable_updateevent()->mutable_event()->set_name("New name");
  registry.function(Request::DataCase::kUpdateEvent)(request, response);

  BOOST_ASSERT(MessageDifferencer::Equals(
    response.updateevent().event(),
    request.updateevent().event()));

  const auto event = events.container.find(
    request.updateevent().event().id());

  BOOST_ASSERT(MessageDifferencer::Equals(
    request.updateevent().event(),
    event->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_handler_negative_test,
  UpdateEventHandlerTestsFixture,
  *description(
    "The update handler should return an error if the event does not exist"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateevent()->mutable_event()->set_id(42);
  Response response;
  registry.function(Request::DataCase::kUpdateEvent)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
