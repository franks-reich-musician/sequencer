#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/event/UpdateSignalTrackAssignmentHandler.h>
#include <sequencer/handler/event/AssignSignalTrackHandler.h>
#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::event;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateSignalTrackAssignmentHandlerTests)


class UpdateSignalTrackAssignmentHandlerTestsFixture {
  public:
    UpdateSignalTrackAssignmentHandlerTestsFixture() {
      registerAssignSignalTrackHandler(registry, events, signalTracks);
      registerUpdateSignalTrackAssignmentHandler(registry, events, signalTracks);
      registerCreateEventHandler(registry, events);
      registerCreateSignalTrackHandler(registry, signalTracks);

      Request request;
      request.mutable_createevent()->mutable_event()->set_name("Test event");
      Response response;
      registry.function(Request::DataCase::kCreateEvent)(request, response);
      eventId = response.createevent().event().id();

      request.mutable_createsignaltrack()->mutable_signaltrack()->set_name("St");
      registry.function(Request::DataCase::kCreateSignalTrack)(
        request, response);
      signalTrackId = response.createsignaltrack().signaltrack().id();

      request.mutable_assignsignaltrack()->set_signaltrackid(signalTrackId);
      request.mutable_assignsignaltrack()->set_eventid(eventId);
      request.mutable_assignsignaltrack()->mutable_assignment()->set_name(
        "Assignment");
      registry.function(Request::DataCase::kAssignSignalTrack)(
        request, response);
    }


    sequencer_key_type signalTrackId;
    sequencer_key_type eventId;
    FunctionRegistry registry;
    events_type events;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  update_signal_track_assignment_handler_test,
  UpdateSignalTrackAssignmentHandlerTestsFixture,
  *description(
    "The update signal track assignment handler should update the attributes"
    "of the assignment."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateassignedsignaltrack()->set_eventid(eventId);
  request.mutable_updateassignedsignaltrack()->set_signaltrackid(signalTrackId);
  request.mutable_updateassignedsignaltrack()->mutable_assignment()->set_name(
    "Updated name");
  Response response;
  registry.function(Request::DataCase::kUpdateAssignedSignalTrack)(
    request, response);

  const auto event = events.container.find(eventId);
  const auto assignment = event->second.container.find(signalTrackId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    assignment->second.attributes,
    request.updateassignedsignaltrack().assignment()));
  BOOST_REQUIRE(MessageDifferencer::Equals(
    assignment->second.attributes,
    response.updateassignedsignaltrack().assignedsignaltrack().attributes()));

  const auto signalTrack = signalTracks.container.find(signalTrackId);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    signalTrack->second.attributes,
    response.updateassignedsignaltrack().assignedsignaltrack().child()));

  BOOST_REQUIRE(MessageDifferencer::Equals(
    event->second.attributes,
    response.updateassignedsignaltrack().assignedsignaltrack().parent()));
}


BOOST_FIXTURE_TEST_CASE(
  update_signal_track_assignment_handler_event_negative_test,
  UpdateSignalTrackAssignmentHandlerTestsFixture,
  *description(
    "The update signal track assignment handler should return an error if the"
    "event does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateassignedsignaltrack()->set_eventid(42);
  request.mutable_updateassignedsignaltrack()->set_signaltrackid(signalTrackId);
  request.mutable_updateassignedsignaltrack()->mutable_assignment()->set_name(
    "Updated name");
  Response response;
  registry.function(Request::DataCase::kUpdateAssignedSignalTrack)(
    request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  update_signal_track_assignment_handler_assignment_negative_test,
  UpdateSignalTrackAssignmentHandlerTestsFixture,
  *description(
    "The update signal track assignment handler should return an error if"
    "the signal track is not assigned to the event."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateassignedsignaltrack()->set_eventid(eventId);
  request.mutable_updateassignedsignaltrack()->set_signaltrackid(42);
  request.mutable_updateassignedsignaltrack()->mutable_assignment()->set_name(
    "Updated name");
  Response response;
  registry.function(Request::DataCase::kUpdateAssignedSignalTrack)(
    request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SIGNAL_TRACK_NOT_ASSIGNED_TO_EVENT);
}


BOOST_AUTO_TEST_SUITE_END()
