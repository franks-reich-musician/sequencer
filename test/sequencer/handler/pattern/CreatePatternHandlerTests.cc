#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/pattern/CreatePatternHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::pattern;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreatePatternHandlerTests)


class CreatePatternHandlerTestsFixture {
  public:
    CreatePatternHandlerTestsFixture() {
      pattern::registerCreatePatternHandler(registry, patterns);
    }


    patterns_type patterns;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  request_handling_test,
  CreatePatternHandlerTestsFixture,
  *description(
    "The create pattern handler should create the pattern in patterns. It also"
    "should fill the response message with the correct values. It should"
    "generate the pattern id."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createpattern()->mutable_pattern()->set_id(5);
  request.mutable_createpattern()->mutable_pattern()->set_name("Test pattern");

  Response response;
  registry.function(Request::DataCase::kCreatePattern)(request, response);

  BOOST_ASSERT(
    response.createpattern().pattern().name().compare("Test pattern") == 0);

  const auto patternId = response.createpattern().pattern().id();
  BOOST_REQUIRE_EQUAL(patterns.container.size(), 1);
  BOOST_REQUIRE_EQUAL(
    patterns.container.at(patternId).attributes.id(),
    patternId);
  BOOST_ASSERT(
    patterns.container.at(patternId).attributes.name().compare("Test pattern") == 0);
}


BOOST_AUTO_TEST_SUITE_END()
