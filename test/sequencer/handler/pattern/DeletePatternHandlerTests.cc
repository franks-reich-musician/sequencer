#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sequencer/handler/pattern/CreatePatternHandler.h>
#include <sequencer/handler/pattern/DeletePatternHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(DeletePatternHandlerTests)


class DeletePatternHandlerTestsFixture {
  public:
    DeletePatternHandlerTestsFixture() {
      pattern::registerCreatePatternHandler(registry, patterns);
      pattern::registerDeletePatternHandler(registry, patterns);
    }


    patterns_type patterns;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  delete_handler_test,
  DeletePatternHandlerTestsFixture,
  *description(
    "The delete handler should delete the pattern."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createpattern()->mutable_pattern()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreatePattern)(request, response);
  BOOST_REQUIRE_EQUAL(patterns.container.size(), 1);

  request.mutable_deletepattern()->set_patternid(
    response.createpattern().pattern().id());
  registry.function(Request::DataCase::kDeletePattern)(request, response);

  BOOST_ASSERT(response.has_deletepattern());
  BOOST_REQUIRE_EQUAL(patterns.container.size(), 0);
}


BOOST_FIXTURE_TEST_CASE(
  delete_handler_negative_test,
  DeletePatternHandlerTestsFixture,
  *description(
    "The delete handler should return an error if the pattern to be deleted"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_deletepattern()->set_patternid(42);
  Response response;
  registry.function(Request::DataCase::kDeletePattern)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
