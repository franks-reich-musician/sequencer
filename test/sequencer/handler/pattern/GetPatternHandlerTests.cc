#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/pattern/CreatePatternHandler.h>
#include <sequencer/handler/pattern/GetPatternHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(GetPatternHandlerTests)


class GetPatternHandlerTestsFixture {
  public:
    GetPatternHandlerTestsFixture():
      patterns()
    {
      pattern::registerCreatePatternHandler(registry, patterns);
      pattern::registerGetPatternHandler(registry, patterns);
    }


    patterns_type patterns;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  request_handling_test,
  GetPatternHandlerTestsFixture,
  *description(
    "The get pattern handler should copy the attributes of the pattern into the"
    "response message."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createpattern()->mutable_pattern()->set_name("Test pattern");

  Response response;
  registry.function(Request::DataCase::kCreatePattern)(request, response);

  const auto patternId = response.createpattern().pattern().id();
  Request getPatternRequest;
  request.mutable_getpattern()->set_patternid(patternId);
  Response getPatternResponse;
  registry.function(Request::DataCase::kGetPattern)(request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createpattern().pattern(),
    getPatternResponse.getpattern().pattern()));
}


BOOST_FIXTURE_TEST_CASE(
  request_handling_negative_test,
  GetPatternHandlerTestsFixture,
  *description(
    "The get pattern handler should return an error if the pattern does not"
    "exists."))
{
  boost::debug::detect_memory_leaks(false);
  Response response;
  Request request;
  request.mutable_getpattern()->set_patternid(42);
  registry.function(Request::DataCase::kGetPattern)(request, response);
  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE(
    response.error().errorcode() ==
      Error::ErrorCode::Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
