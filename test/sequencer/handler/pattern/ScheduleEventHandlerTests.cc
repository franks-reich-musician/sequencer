#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/pattern/ScheduleEventHandler.h>
#include <sequencer/handler/pattern/CreatePatternHandler.h>
#include <sequencer/handler/event/CreateEventHandler.h>
#include <sequencer/music/Time.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::time;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace fr::musician::data_definition::events;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(ScheduleEventHandlerTests)


class ScheduleEventHandlerTestsFixture {
  public:
    ScheduleEventHandlerTestsFixture() {
      pattern::registerScheduleEventHandler(registry, patterns, events);
      pattern::registerCreatePatternHandler(registry, patterns);
      event::registerCreateEventHandler(registry, events);

      for (size_t i = 0; i < 10; i++) {
        Request request;
        request.mutable_createevent()->mutable_event()->set_name("Test event");
        request.mutable_createevent()->mutable_event()->mutable_noteevent();
        Response response;
        registry(Request::DataCase::kCreateEvent)(request, response);
        eventIds.push_back(response.createevent().event().id());
      }

      Request request;
      request.mutable_createpattern()->mutable_pattern()->set_name(
        "Test pattern");
      request.mutable_createpattern()->mutable_pattern()->set_basedivision(2);
      request.mutable_createpattern()->mutable_pattern()->set_resolution(2048);
      auto &length =
        *request.mutable_createpattern()->mutable_pattern()->mutable_length();
      length.add_denominators(1);
      length.add_denominators(2);
      length.add_denominators(4);
      length.add_denominators(8);
      length.add_divisions(4);
      length.add_divisions(0);
      length.add_divisions(0);
      length.add_divisions(0);
      length.set_resolution(2048);
      length.set_remainder(1);
      Response response;
      registry(Request::DataCase::kCreatePattern)(request, response);
      patternId = response.createpattern().pattern().id();
    }


    sequencer_key_type patternId;
    vector<sequencer_key_type> eventIds;
    patterns_type patterns;
    events_type events;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    "The schedule event handler should schedule an event with the pattern."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(eventIds.at(3));
  scheduleEventRequest.set_patternid(patternId);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(2);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(2);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(2);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  const auto internalBeginPosition = convertMusicTime(beginPosition);
  const auto internalEndPosition = convertMusicTime(endPosition);
  const auto internalPosition = make_tuple(
    internalBeginPosition, internalEndPosition, 1, Notes::E);
  const auto &pattern = patterns.container.at(patternId);
  BOOST_REQUIRE_EQUAL(pattern.container.count(internalPosition), 1);

  const auto &eventId = pattern.container.at(internalPosition);
  BOOST_REQUIRE_EQUAL(eventId, eventIds.at(3));

  const auto actualScheduledEvent = response.scheduleevent().scheduledevent();
  const auto &event = events.container.at(eventIds.at(3));
  BOOST_REQUIRE(MessageDifferencer::Equals(
    event.attributes, actualScheduledEvent.event()));
  BOOST_REQUIRE(MessageDifferencer::Equals(
    pattern.attributes, actualScheduledEvent.pattern()));
  BOOST_REQUIRE(MessageDifferencer::Equals(
    beginPosition, actualScheduledEvent.position().begin()));
  BOOST_REQUIRE(MessageDifferencer::Equals(
    endPosition, actualScheduledEvent.position().end()));
}


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_pattern_negative_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    "The schedule event handler should return an error if the pattern does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(eventIds.at(3));
  scheduleEventRequest.set_patternid(42);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(2);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(2);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(2);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_event_negative_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    "The schedule event handler should return an error if the event does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(4200);
  scheduleEventRequest.set_patternid(patternId);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(2);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(2);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(2);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_EVENT_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_begin_position_base_negative_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    "The schedule event handler should return an error if the begin position"
    "does not have the same base as the pattern."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(eventIds.at(3));
  scheduleEventRequest.set_patternid(patternId);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(3);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(2);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(2);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_BEGIN_POSITION_BASE_ERROR);
}


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_end_position_base_negative_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    "The schedule event handler should return an error if the end position"
    "does not have the same base as the pattern."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(eventIds.at(3));
  scheduleEventRequest.set_patternid(patternId);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(2);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(3);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(2);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_END_POSITION_BASE_ERROR);
}


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_position_overlap_negative_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    "The schedule event handler should return an error if the new event would"
    "overlap with already scheduled events."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(eventIds.at(3));
  scheduleEventRequest.set_patternid(patternId);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(2);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(2);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(2);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  beginPosition.set_divisions(0, 0);
  registry(Request::kScheduleEvent)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_POSITION_OVERLAP_ERROR);
}


BOOST_FIXTURE_TEST_CASE(
  schedule_event_handler_negative_range_negative_test,
  ScheduleEventHandlerTestsFixture,
  *description(
    ""))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  auto &scheduleEventRequest = *request.mutable_scheduleevent();
  scheduleEventRequest.set_eventid(eventIds.at(3));
  scheduleEventRequest.set_patternid(patternId);
  auto &eventPosition = *scheduleEventRequest.mutable_position();
  eventPosition.set_octave(1);
  eventPosition.set_note(Notes::E);
  auto &beginPosition = *eventPosition.mutable_begin();
  beginPosition.set_resolution(2048);
  beginPosition.add_denominators(1);
  beginPosition.add_denominators(2);
  beginPosition.add_denominators(4);
  beginPosition.add_denominators(8);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(1);
  beginPosition.add_divisions(0);
  beginPosition.add_divisions(1);
  beginPosition.set_remainder(1);
  auto &endPosition = *eventPosition.mutable_end();
  endPosition.set_resolution(2048);
  endPosition.add_denominators(1);
  endPosition.add_denominators(2);
  endPosition.add_denominators(4);
  endPosition.add_denominators(8);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(0);
  endPosition.add_divisions(1);
  endPosition.set_remainder(0);
  Response response;
  registry(Request::kScheduleEvent)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_NEGATIVE_RANGE_ERROR);
}


BOOST_AUTO_TEST_SUITE_END()
