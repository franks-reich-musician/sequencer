#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/pattern/CreatePatternHandler.h>
#include <sequencer/handler/pattern/UpdatePatternHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdatePatternHandlerTests)


class UpdatePatternHandlerTestsFixture {
  public:
    UpdatePatternHandlerTestsFixture() {
      pattern::registerCreatePatternHandler(registry, patterns);
      pattern::registerUpdatePatternHandler(registry, patterns);
    }

    ~UpdatePatternHandlerTestsFixture() {}


    patterns_type patterns;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  update_handling_test,
  UpdatePatternHandlerTestsFixture,
  *description(
    "The update request handler should update the pattern."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createpattern()->mutable_pattern()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreatePattern)(request, response);

  *request.mutable_updatepattern()->mutable_pattern() =
    response.mutable_createpattern()->pattern();
  request.mutable_updatepattern()->mutable_pattern()->set_name("New Name");
  registry.function(Request::DataCase::kUpdatePattern)(request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatepattern().pattern(),
    response.updatepattern().pattern()));

  const auto pattern = patterns.container.find(
    request.updatepattern().pattern().id());

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatepattern().pattern(),
    pattern->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_handling_negative_test,
  UpdatePatternHandlerTestsFixture,
  *description(
    "The update handler should return an error if the pattern does not "
    "exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_updatepattern()->mutable_pattern()->set_id(43);
  Response response;
  registry.function(Request::DataCase::kUpdatePattern)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_PATTERN_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
