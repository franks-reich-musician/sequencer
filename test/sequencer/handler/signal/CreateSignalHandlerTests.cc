#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal/CreateSignalHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreateSignalHandlerTests)


class CreateSignalHandlerTestsFixture {
  public:
    CreateSignalHandlerTestsFixture() {
      signal::registerCreateSignalHandler(registry, signals);
    }

    FunctionRegistry registry;
    signals_type signals;
};


BOOST_FIXTURE_TEST_CASE(
  create_handler_test,
  CreateSignalHandlerTestsFixture,
  *description(
    "The signal create handler should create a signal."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignal()->mutable_signal()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignal)(request, response);

  request.mutable_createsignal()->mutable_signal()->set_id(
    response.createsignal().signal().id());
  BOOST_REQUIRE_EQUAL(
    MessageDifferencer::Equals(
      request.createsignal().signal(),
      response.createsignal().signal()), true);

  const auto signal = signals.container.find(response.createsignal().signal().id());
  BOOST_REQUIRE_EQUAL(
    MessageDifferencer::Equals(
      response.createsignal().signal(),
      signal->second.attributes), true);
}


BOOST_AUTO_TEST_SUITE_END()
