#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal/CreateSignalHandler.h>
#include <sequencer/handler/signal/DeleteSignalHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(DeleteSignalHandlerTests)


class DeleteSignalHandlerTestsFixture {
  public:
    DeleteSignalHandlerTestsFixture() {
      signal::registerCreateSignalHandler(registry, signals);
      signal::registerDeleteSignalHandler(registry, signals);
    }

    FunctionRegistry registry;
    signals_type signals;
};


BOOST_FIXTURE_TEST_CASE(
  delete_handler_test,
  DeleteSignalHandlerTestsFixture,
  *description(
    "The delete handler should delete the signal if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignal()->mutable_signal()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignal)(request, response);

  BOOST_REQUIRE_EQUAL(signals.container.size(), 1);

  request.mutable_deletesignal()->set_signalid(
    response.createsignal().signal().id());
  registry.function(Request::DataCase::kDeleteSignal)(request, response);

  BOOST_REQUIRE_EQUAL(signals.container.size(), 0);
  BOOST_ASSERT(response.has_deletesignal());
}


BOOST_FIXTURE_TEST_CASE(
  delete_handler_negative_test,
  DeleteSignalHandlerTestsFixture,
  *description(
    "The delete signal handler should return an error if the signal does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_deletesignal()->set_signalid(42);
  Response response;
  registry.function(Request::DataCase::kDeleteSignal)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SIGNAL_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
