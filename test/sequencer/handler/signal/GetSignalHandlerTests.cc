#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal/CreateSignalHandler.h>
#include <sequencer/handler/signal/GetSignalHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(GetSignalHandlerTests)


class GetSignalHandlerTestsFixture {
  public:
    GetSignalHandlerTestsFixture() {
      signal::registerCreateSignalHandler(registry, signals);
      signal::registerGetSignalHandler(registry, signals);
    }

    FunctionRegistry registry;
    signals_type signals;
};


BOOST_FIXTURE_TEST_CASE(
  get_handler_test,
  GetSignalHandlerTestsFixture,
  *description(
    "The get handler should return the signal if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignal()->mutable_signal()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignal)(request, response);

  request.mutable_getsignal()->set_signalid(
    response.createsignal().signal().id());
  Response getResponse;
  registry.function(Request::DataCase::kGetSignal)(request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createsignal().signal(),
    getResponse.getsignal().signal()));
}


BOOST_FIXTURE_TEST_CASE(
  get_handler_negative_test,
  GetSignalHandlerTestsFixture,
  *description(
    "The get handler should return an error if it does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_getsignal()->set_signalid(42);
  Response response;
  registry.function(Request::DataCase::kGetSignal)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SIGNAL_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
