#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal/CreateSignalHandler.h>
#include <sequencer/handler/signal/UpdateSignalHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateSignalHandlerTests)


class UpdateSignalHandlerTestsFixture {
  public:
    UpdateSignalHandlerTestsFixture() {
      signal::registerCreateSignalHandler(registry, signals);
      signal::registerUpdateSignalHandler(registry, signals);
    }

    FunctionRegistry registry;
    signals_type signals;
};


BOOST_FIXTURE_TEST_CASE(
  update_handler_test,
  UpdateSignalHandlerTestsFixture,
  *description(
    "The update handler should update the signal if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignal()->mutable_signal()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignal)(request, response);

  *request.mutable_updatesignal()->mutable_signal() =
    response.createsignal().signal();
  request.mutable_updatesignal()->mutable_signal()->set_name("New");
  Response updateResponse;
  registry.function(Request::DataCase::kUpdateSignal)(request, updateResponse);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatesignal().signal(),
    updateResponse.updatesignal().signal()));

  const auto signal = signals.container.find(
    response.createsignal().signal().id());

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatesignal().signal(),
    signal->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_handler_negative_test,
  UpdateSignalHandlerTestsFixture,
  *description(
    "The update handler should return an error if the signal does not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_updatesignal()->mutable_signal()->set_id(42);
  Response response;
  registry.function(Request::DataCase::kUpdateSignal)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SIGNAL_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
