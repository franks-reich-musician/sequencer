#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreateSignalTrackHandlerTests)


class CreateSignalTrackHandlerTestsFixture {
  public:
    CreateSignalTrackHandlerTestsFixture() {
      signal_track::registerCreateSignalTrackHandler(registry, signalTracks);
    }

    FunctionRegistry registry;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  create_handler_test,
  CreateSignalTrackHandlerTestsFixture,
  *description(
    "The create handler should create a signal track"))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignaltrack()->mutable_signaltrack()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignalTrack)(request, response);

  BOOST_REQUIRE_EQUAL(signalTracks.container.size(), 1);

  const auto signalTrack = signalTracks.container.find(
    response.createsignaltrack().signaltrack().id());
  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createsignaltrack().signaltrack(),
    signalTrack->second.attributes));
}


BOOST_AUTO_TEST_SUITE_END()
