#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>
#include <sequencer/handler/signal_track/DeleteSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(DeleteSignalTrackHandlerTests)


class DeleteSignalTrackHandlerTestsFixture {
  public:
    DeleteSignalTrackHandlerTestsFixture() {
      signal_track::registerCreateSignalTrackHandler(registry, signalTracks);
      signal_track::registerDeleteSignalTrackHandler(registry, signalTracks);
    }

    FunctionRegistry registry;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  delete_handler_test,
  DeleteSignalTrackHandlerTestsFixture,
  *description(
    "The delete handler should delete a signal track if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignaltrack()->mutable_signaltrack()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignalTrack)(request, response);

  BOOST_REQUIRE_EQUAL(signalTracks.container.size(), 1);

  request.mutable_deletesignaltrack()->set_signaltrackid(
    response.createsignaltrack().signaltrack().id());
  registry.function(Request::DataCase::kDeleteSignalTrack)(request, response);

  BOOST_REQUIRE_EQUAL(signalTracks.container.size(), 0);
  BOOST_REQUIRE(response.has_deletesignaltrack());
}


BOOST_FIXTURE_TEST_CASE(
  delete_handler_negative_test,
  DeleteSignalTrackHandlerTestsFixture,
  *description(
    "The delete handler should return an error if the signal track does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_deletesignaltrack()->set_signaltrackid(42);
  Response response;
  registry.function(Request::DataCase::kDeleteSignalTrack)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(), Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
