#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>
#include <sequencer/handler/signal_track/GetSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(GetSignalTrackHandlerTests)


class GetSignalTrackHandlerTestsFixture {
  public:
    GetSignalTrackHandlerTestsFixture() {
      signal_track::registerCreateSignalTrackHandler(registry, signalTracks);
      signal_track::registerGetSignalTrackHandler(registry, signalTracks);
    }

    FunctionRegistry registry;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  get_handler_test,
  GetSignalTrackHandlerTestsFixture,
  *description(
    "The get handler should return the signal track if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignaltrack()->mutable_signaltrack()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignalTrack)(request, response);

  request.mutable_getsignaltrack()->set_signaltrackid(
    response.createsignaltrack().signaltrack().id());
  Response getResponse;
  registry.function(Request::DataCase::kGetSignalTrack)(request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createsignaltrack().signaltrack(),
    getResponse.createsignaltrack().signaltrack()));
}


BOOST_FIXTURE_TEST_CASE(
  gat_handler_negative_test,
  GetSignalTrackHandlerTestsFixture,
  *description(
    "The get handler should return an error if the signal track does not"
    "exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_getsignaltrack()->set_signaltrackid(42);
  Response response;
  registry.function(Request::DataCase::kGetSignalTrack)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
