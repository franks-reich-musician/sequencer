#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/handler/signal_track/CreateSignalTrackHandler.h>
#include <sequencer/handler/signal_track/UpdateSignalTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::signal_track;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateSignalTrackHandlerTests)


class UpdateSignalTrackHandlerTestsFixture {
  public:
    UpdateSignalTrackHandlerTestsFixture() {
      signal_track::registerCreateSignalTrackHandler(registry, signalTracks);
      signal_track::registerUpdateSignalHandler(registry, signalTracks);
    }

    FunctionRegistry registry;
    signal_tracks_type signalTracks;
};


BOOST_FIXTURE_TEST_CASE(
  update_handler_test,
  UpdateSignalTrackHandlerTestsFixture,
  *description(
    "The update handler should update the signal track if it exists."))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_createsignaltrack()->mutable_signaltrack()->set_name("Test");
  Response response;
  registry.function(Request::DataCase::kCreateSignalTrack)(request, response);

  *request.mutable_updatesignaltrack()->mutable_signaltrack() =
    response.createsignaltrack().signaltrack();
  request.mutable_updatesignaltrack()->mutable_signaltrack()->set_name("New");
  registry.function(Request::DataCase::kUpdateSignalTrack)(request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatesignaltrack().signaltrack(),
    response.updatesignaltrack().signaltrack()));

  const auto signalTrack = signalTracks.container.find(
    request.updatesignaltrack().signaltrack().id());

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatesignaltrack().signaltrack(),
    signalTrack->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_handler_negative_test,
  UpdateSignalTrackHandlerTestsFixture,
  *description(
    "The update handler should return an error if the signal track does"
    "not exist"))
{
  boost::debug::detect_memory_leaks(false);
  Request request;
  request.mutable_updatesignaltrack()->mutable_signaltrack()->set_id(42);
  Response response;
  registry.function(Request::DataCase::kUpdateSignalTrack)(request, response);

  BOOST_REQUIRE(response.has_error());
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SIGNAL_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
