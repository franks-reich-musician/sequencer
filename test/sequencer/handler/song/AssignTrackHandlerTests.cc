#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Request.pb.h>
#include <Response.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/song/AssignTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(AssignTrackHandlerTests)


class AssignTrackHandlerTestsFixture {
  public:
    AssignTrackHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      track::registerCreateTrackHandler(registry, tracks);
      song::registerAssignTrackHandler(registry, songs, tracks);

      Request request;
      request.mutable_createsong()->mutable_song()->set_name("Song 1");
      Response response;
      registry.function(Request::DataCase::kCreateSong)(request, response);
      songId = response.createsong().song().id();

      request.mutable_createtrack()->mutable_track()->set_name("Track");
      registry.function(Request::DataCase::kCreateTrack)(request, response);
      trackId = response.createtrack().track().id();
    }


    sequencer_key_type trackId;
    sequencer_key_type songId;
    tracks_type tracks;
    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  assign_track_handler_test,
  AssignTrackHandlerTestsFixture,
  *description(
    "The assign track handler should assign a track to a song if both exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_assigntrack()->set_songid(songId);
  request.mutable_assigntrack()->set_trackid(trackId);
  request.mutable_assigntrack()->mutable_assignment()->set_name("Assignment");
  Response response;
  registry.function(Request::DataCase::kAssignTrack)(request, response);

  const auto song = songs.container.find(songId);
  const auto track = tracks.container.find(trackId);
  const auto assignment = song->second.container.find(trackId);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    song->second.attributes,
    response.assigntrack().assignedtrack().parent()));

  BOOST_REQUIRE(MessageDifferencer::Equals(
    track->second.attributes,
    response.assigntrack().assignedtrack().child()));

  BOOST_REQUIRE(MessageDifferencer::Equals(
    assignment->second.attributes,
    response.assigntrack().assignedtrack().attributes()));
}


BOOST_FIXTURE_TEST_CASE(
  assign_track_handler_song_negative_test,
  AssignTrackHandlerTestsFixture,
  *description(
    "The assign track handler should return an error if the song"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_assigntrack()->set_songid(42);
  request.mutable_assigntrack()->set_trackid(trackId);
  request.mutable_assigntrack()->mutable_assignment();
  Response response;
  registry.function(Request::DataCase::kAssignTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  assign_track_handler_track_negative_test,
  AssignTrackHandlerTestsFixture,
  *description(
    "The assign track handler should return an error if the track"
    "does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_assigntrack()->set_songid(songId);
  request.mutable_assigntrack()->set_trackid(42);
  request.mutable_assigntrack()->mutable_assignment();
  Response response;
  registry.function(Request::DataCase::kAssignTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
