#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreateSongHandlerTests)


class CreateSongHandlerTestsFixture {
  public:
    CreateSongHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
    }


    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  create_handler_test,
  CreateSongHandlerTestsFixture,
  *description(
    "The create handler should create a song"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createsong()->mutable_song()->set_name("Test song");
  Response response;
  registry.function(Request::DataCase::kCreateSong)(request, response);

  BOOST_REQUIRE_EQUAL(songs.container.size(), 1);

  const auto song = songs.container.find(response.createsong().song().id());

  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createsong().song(), song->second.attributes));
}


BOOST_AUTO_TEST_SUITE_END()
