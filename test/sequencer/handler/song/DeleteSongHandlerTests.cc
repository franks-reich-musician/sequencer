#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/song/DeleteSongHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(DeleteSongHandlerTests)


class DeleteSongHandlerTestsFixture {
  public:
    DeleteSongHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      song::registerDeleteSongHandler(registry, songs);
    }


    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  delete_handler_test,
  DeleteSongHandlerTestsFixture,
  *description(
    "The delete handler should delete the song if exists."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createsong()->mutable_song()->set_name("Test song");
  Response response;
  registry.function(Request::DataCase::kCreateSong)(request, response);

  BOOST_REQUIRE_EQUAL(songs.container.size(), 1);

  request.mutable_deletesong()->set_songid(response.createsong().song().id());
  registry.function(Request::DataCase::kDeleteSong)(request, response);

  BOOST_REQUIRE(response.has_deletesong());
  BOOST_REQUIRE_EQUAL(songs.container.size(), 0);
}


BOOST_FIXTURE_TEST_CASE(
  delete_handler_negative_test,
  DeleteSongHandlerTestsFixture,
  *description(
    "The delete handler should return an error if the sond does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_deletesong()->set_songid(42);
  Response response;
  registry.function(Request::DataCase::kDeleteSong)(request, response);

  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
