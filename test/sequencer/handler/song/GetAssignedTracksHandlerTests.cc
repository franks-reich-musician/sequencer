#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sstream>


#include <google/protobuf/util/message_differencer.h>


#include <Request.pb.h>
#include <Response.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/song/AssignTrackHandler.h>
#include <sequencer/handler/song/GetAssignedTracksHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(GetAssignedTracksHandlerTests)


class GetAssignedTracksHandlerTestsFixture {
  public:
    GetAssignedTracksHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      track::registerCreateTrackHandler(registry, tracks);
      song::registerAssignTrackHandler(registry, songs, tracks);
      song::registerGetAssignedTracksHandler(registry, songs, tracks);

      Request request;
      request.mutable_createsong()->mutable_song()->set_name("Song 1");
      Response response;
      registry.function(Request::DataCase::kCreateSong)(request, response);
      songId = response.createsong().song().id();

      for (size_t i = 0; i < 5; i++) {
        stringstream ss;
        ss << "Track " << i;
        request.mutable_createtrack()->mutable_track()->set_name(ss.str());
        registry.function(Request::DataCase::kCreateTrack)(request, response);
        const auto trackId = response.createtrack().track().id();
        trackIds.push_back(trackId);

        request.mutable_assigntrack()->set_songid(songId);
        request.mutable_assigntrack()->set_trackid(trackId);
        ss << " Assignment";
        request.mutable_assigntrack()->mutable_assignment()->set_name(ss.str());
        registry.function(Request::DataCase::kAssignTrack)(request, response);
      }
    }


    vector<sequencer_key_type> trackIds;
    sequencer_key_type songId;
    tracks_type tracks;
    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  get_assignment_handler_test,
  GetAssignedTracksHandlerTestsFixture,
  *description(
    "The get assignment handler should return the song, the track and"
    "the assignment."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_getassignedtracks()->set_songid(songId);
  Response response;
  registry.function(Request::DataCase::kGetAssignedTracks)(request, response);
  const auto numAssignments = response.getassignedtracks().tracks_size();
  BOOST_REQUIRE_EQUAL(numAssignments, 5);

  const auto song = songs.container.find(songId);
  size_t index = 0;
  for (const auto &track: tracks.container) {
    const auto assignment = song->second.container.find(track.first);
    AssignedTrack assignedTrack;
    *assignedTrack.mutable_parent() = song->second.attributes;
    *assignedTrack.mutable_child() = track.second.attributes;
    *assignedTrack.mutable_attributes() = assignment->second.attributes;
    BOOST_REQUIRE(MessageDifferencer::Equals(
      assignedTrack,
      response.getassignedtracks().tracks(index)));
    index++;
  }
}


BOOST_FIXTURE_TEST_CASE(
  get_assignment_handler_song_negative_test,
  GetAssignedTracksHandlerTestsFixture,
  *description(
    "The get assignment handler should return an error if the song does"
    "not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_getassignedtracks()->set_songid(42);
  Response response;
  registry.function(Request::DataCase::kGetAssignedTracks)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
