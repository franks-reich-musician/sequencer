#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/song/GetSongHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(GetSongHandlerTests)


class GetSongHandlerTestsFixture {
  public:
    GetSongHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      song::registerGetSongHandler(registry, songs);
    }


    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  get_handler_test,
  GetSongHandlerTestsFixture,
  *description(
    "The handler should get song if it exists."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createsong()->mutable_song()->set_name("Test song");
  Response response;
  registry.function(Request::DataCase::kCreateSong)(request, response);

  request.mutable_getsong()->set_songid(response.createsong().song().id());
  Response getResponse;
  registry.function(Request::DataCase::kGetSong)(request, getResponse);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createsong().song(),
    getResponse.getsong().song()));
}


BOOST_FIXTURE_TEST_CASE(
  het_handler_negative_test,
  GetSongHandlerTestsFixture,
  *description(
    "The get handler should return an error if the song does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_getsong()->set_songid(42);
  Response response;
  registry.function(Request::DataCase::kGetSong)(request, response);

  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
