#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sstream>


#include <google/protobuf/util/message_differencer.h>


#include <Request.pb.h>
#include <Response.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/song/AssignTrackHandler.h>
#include <sequencer/handler/song/RemoveTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(RemoveTrackHandlerTests)


class RemoveTrackHandlerTestsFixture {
  public:
    RemoveTrackHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      track::registerCreateTrackHandler(registry, tracks);
      song::registerAssignTrackHandler(registry, songs, tracks);
      song::registerRemoveTrackHandler(registry, songs);

      Request request;
      request.mutable_createsong()->mutable_song()->set_name("Song 1");
      Response response;
      registry.function(Request::DataCase::kCreateSong)(request, response);
      songId = response.createsong().song().id();

      for (size_t i = 0; i < 5; i++) {
        stringstream ss;
        ss << "Track " << i;
        request.mutable_createtrack()->mutable_track()->set_name(ss.str());
        registry.function(Request::DataCase::kCreateTrack)(request, response);
        const auto trackId = response.createtrack().track().id();
        trackIds.push_back(trackId);

        request.mutable_assigntrack()->set_songid(songId);
        request.mutable_assigntrack()->set_trackid(trackId);
        ss << " Assignment";
        request.mutable_assigntrack()->mutable_assignment()->set_name(ss.str());
        registry.function(Request::DataCase::kAssignTrack)(request, response);
      }
    }


    vector<sequencer_key_type> trackIds;
    sequencer_key_type songId;
    tracks_type tracks;
    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  remove_track_handler_test,
  RemoveTrackHandlerTestsFixture,
  *description(
    "The remove track handler should remove the assignment from the song."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_removetrack()->set_songid(songId);
  const auto deletedTrackId = trackIds.at(3);
  request.mutable_removetrack()->set_trackid(deletedTrackId);
  Response response;
  registry.function(Request::DataCase::kRemoveTrack)(request, response);

  const auto song = songs.container.find(songId);
  BOOST_REQUIRE_EQUAL(song->second.container.size(), 4);

  const vector<sequencer_key_type> expectedTrackIds{
    trackIds.at(0), trackIds.at(1), trackIds.at(2), trackIds.at(4)};
  vector<sequencer_key_type> actualTrackIds{};
  for (const auto &track: song->second.container) {
    actualTrackIds.push_back(track.first);
  }

  BOOST_REQUIRE_EQUAL_COLLECTIONS(
    actualTrackIds.cbegin(), actualTrackIds.cend(),
    expectedTrackIds.cbegin(), expectedTrackIds.cend());

  BOOST_REQUIRE(response.has_removetrack());
}


BOOST_FIXTURE_TEST_CASE(
  remove_track_handler_song_negative_test,
  RemoveTrackHandlerTestsFixture,
  *description(
    "The remove track handler should return an error if the song does not"
    "exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_removetrack()->set_songid(42);
  const auto deletedTrackId = trackIds.at(3);
  request.mutable_removetrack()->set_trackid(deletedTrackId);
  Response response;
  registry.function(Request::DataCase::kRemoveTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  remove_track_handler_assignment_negative_test,
  RemoveTrackHandlerTestsFixture,
  *description(
    "The remove track handler should return an error if the track is not"
    "assigned to the song."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_removetrack()->set_songid(songId);
  request.mutable_removetrack()->set_trackid(42);
  Response response;
  registry.function(Request::DataCase::kRemoveTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_TRACK_NOT_ASSIGNED_TO_SONG);
}


BOOST_AUTO_TEST_SUITE_END()
