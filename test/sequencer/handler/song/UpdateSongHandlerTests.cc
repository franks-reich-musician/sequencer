#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/song/UpdateSongHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateSongHandlerTests)


class UpdateSongHandlerTestsFixture {
  public:
    UpdateSongHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      song::registerUpdateSongHandler(registry, songs);
    }


    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  update_handler_test,
  UpdateSongHandlerTestsFixture,
  *description(
    "The update handler should update the song if it exists."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createsong()->mutable_song()->set_name("Test song");
  Response response;
  registry.function(Request::DataCase::kCreateSong)(request, response);

  *request.mutable_updatesong()->mutable_song() = response.createsong().song();
  request.mutable_updatesong()->mutable_song()->set_name("New name");
  registry.function(Request::DataCase::kUpdateSong)(request, response);

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatesong().song(), response.updatesong().song()));

  const auto song = songs.container.find(
    request.updatesong().song().id());

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updatesong().song(),
    song->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_handler_negative_test,
  UpdateSongHandlerTestsFixture,
  *description(
    "The update handler should return an error if the song does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updatesong()->mutable_song()->set_id(42);
  Response response;
  registry.function(Request::DataCase::kUpdateSong)(request, response);

  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
