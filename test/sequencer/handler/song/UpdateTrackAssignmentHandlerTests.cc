#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Request.pb.h>
#include <Response.pb.h>


#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/song/CreateSongHandler.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/song/AssignTrackHandler.h>
#include <sequencer/handler/song/UpdateTrackAssignmentHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(UpdateTrackAssignmentHandlerTests)


class UpdateTrackAssignmentHandlerTestsFixture {
  public:
    UpdateTrackAssignmentHandlerTestsFixture() {
      song::registerCreateSongHandler(registry, songs);
      track::registerCreateTrackHandler(registry, tracks);
      song::registerAssignTrackHandler(registry, songs, tracks);
      song::registerUpdateTrackAssignmentHandler(registry, songs, tracks);

      Request request;
      request.mutable_createsong()->mutable_song()->set_name("Song 1");
      Response response;
      registry.function(Request::DataCase::kCreateSong)(request, response);
      songId = response.createsong().song().id();

      request.mutable_createtrack()->mutable_track()->set_name("Track");
      registry.function(Request::DataCase::kCreateTrack)(request, response);
      trackId = response.createtrack().track().id();

      request.mutable_assigntrack()->set_songid(songId);
      request.mutable_assigntrack()->set_trackid(trackId);
      request.mutable_assigntrack()->mutable_assignment()->set_name("Assignment");
      registry.function(Request::DataCase::kAssignTrack)(request, response);
    }


    sequencer_key_type trackId;
    sequencer_key_type songId;
    tracks_type tracks;
    songs_type songs;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  update_track_assignment_handler_test,
  UpdateTrackAssignmentHandlerTestsFixture,
  *description(
    "The update track assignment handler should update the attributes of"
    "the assignment."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateassignedtrack()->set_songid(songId);
  request.mutable_updateassignedtrack()->set_trackid(trackId);
  request.mutable_updateassignedtrack()->mutable_assignment()->set_name("A");
  Response response;
  registry.function(Request::DataCase::kUpdateAssignedTrack)(request, response);

  const auto song = songs.container.find(songId);
  const auto track = tracks.container.find(trackId);
  const auto assignment = song->second.container.find(trackId);
  AssignedTrack expectedTrack;
  *expectedTrack.mutable_parent() = song->second.attributes;
  *expectedTrack.mutable_child() = track->second.attributes;
  *expectedTrack.mutable_attributes() = assignment->second.attributes;
  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.updateassignedtrack().assignedtrack(),
    expectedTrack));

  BOOST_REQUIRE(MessageDifferencer::Equals(
    request.updateassignedtrack().assignment(),
    expectedTrack.attributes()));
}


BOOST_FIXTURE_TEST_CASE(
  update_track_assignment_handler_song_negative_test,
  UpdateTrackAssignmentHandlerTestsFixture,
  *description(
    "The update track assignment handler should return an error if the"
    "song does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateassignedtrack()->set_songid(42);
  request.mutable_updateassignedtrack()->set_trackid(trackId);
  request.mutable_updateassignedtrack()->mutable_assignment()->set_name("A");
  Response response;
  registry.function(Request::DataCase::kUpdateAssignedTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_SONG_DOES_NOT_EXIST);
}


BOOST_FIXTURE_TEST_CASE(
  update_track_assignment_handler_assignment_negative_test,
  UpdateTrackAssignmentHandlerTestsFixture,
  *description(
    "The update track assignment handler should return an error if the"
    "track is not assigned to the song."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updateassignedtrack()->set_songid(songId);
  request.mutable_updateassignedtrack()->set_trackid(42);
  request.mutable_updateassignedtrack()->mutable_assignment()->set_name("A");
  Response response;
  registry.function(Request::DataCase::kUpdateAssignedTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_TRACK_NOT_ASSIGNED_TO_SONG);
}


BOOST_AUTO_TEST_SUITE_END()
