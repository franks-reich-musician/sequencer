#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/track/CreateTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(CreateTrackHandlerTests)


class CreateTrackHandlerTestsFixture {
  public:
    CreateTrackHandlerTestsFixture() {
      track::registerCreateTrackHandler(registry, tracks);
    }


    tracks_type tracks;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  create_handler_test,
  CreateTrackHandlerTestsFixture,
  *description(
  "The create track handler should create a track."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createtrack()->mutable_track()->set_name("Test track");
  Response response;
  registry.function(Request::DataCase::kCreateTrack)(request, response);

  request.mutable_createtrack()->mutable_track()->set_id(
    response.createtrack().track().id());

  BOOST_ASSERT(MessageDifferencer::Equals(
    response.createtrack().track(),
    request.createtrack().track()));
  BOOST_REQUIRE_EQUAL(tracks.container.size(), 1);

  const auto track = tracks.container.find(response.createtrack().track().id());
  BOOST_ASSERT(MessageDifferencer::Equals(
    request.createtrack().track(),
    track->second.attributes));
}


BOOST_AUTO_TEST_SUITE_END()
