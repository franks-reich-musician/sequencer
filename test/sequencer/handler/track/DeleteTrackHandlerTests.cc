#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/track/DeleteTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(DeleteTrackHandlerTests)


class DeleteTrackHandlerTestsFixture {
  public:
    DeleteTrackHandlerTestsFixture() {
      track::registerCreateTrackHandler(registry, tracks);
      track::registerDeleteTrackHandler(registry, tracks);
    }


    tracks_type tracks;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  delete_handler_test,
  DeleteTrackHandlerTestsFixture,
  *description(
    "The delete handler should remove an existing track."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createtrack()->mutable_track()->set_name("Test track");
  Response response;
  registry.function(Request::DataCase::kCreateTrack)(request, response);

  BOOST_REQUIRE_EQUAL(tracks.container.size(), 1);

  request.mutable_deletetrack()->set_trackid(
    response.createtrack().track().id());
  registry.function(Request::DataCase::kDeleteTrack)(request, response);

  BOOST_REQUIRE_EQUAL(tracks.container.size(), 0);
  BOOST_REQUIRE(response.has_deletetrack());
}


BOOST_FIXTURE_TEST_CASE(
  delete_handler_negative_test,
  DeleteTrackHandlerTestsFixture,
  *description(
    "The delete handler should return an error if the track does not exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_deletetrack()->set_trackid(42);
  Response response;
  registry.function(Request::DataCase::kDeleteTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
