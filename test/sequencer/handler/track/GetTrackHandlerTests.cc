#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/track/GetTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(GetTrackHandlerTests)


class GetTrackHandlerTestsFixture {
  public:
    GetTrackHandlerTestsFixture() {
      track::registerCreateTrackHandler(registry, tracks);
      track::registerGetTrackHandler(registry, tracks);
    }


    tracks_type tracks;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  get_track_handler_test,
  GetTrackHandlerTestsFixture,
  *description(
    "The get track handler should return the track if exists."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createtrack()->mutable_track()->set_name("Test track");
  Response response;
  registry.function(Request::DataCase::kCreateTrack)(request, response);

  Response getResponse;
  request.mutable_gettrack()->set_trackid(
    response.mutable_createtrack()->track().id());
  registry.function(Request::DataCase::kGetTrack)(request, getResponse);
  BOOST_REQUIRE(MessageDifferencer::Equals(
    response.createtrack().track(),
    getResponse.gettrack().track()));
}


BOOST_FIXTURE_TEST_CASE(
  get_track_handler_negative_test,
  GetTrackHandlerTestsFixture,
  *description(
    "The get track handler should return an error if it does not exist"))
{
  Request request;
  request.mutable_gettrack()->set_trackid(42);
  Response response;
  registry.function(Request::DataCase::kGetTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
