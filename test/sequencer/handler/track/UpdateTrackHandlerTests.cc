#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <Response.pb.h>
#include <sequencer/Typedef.h>
#include <sequencer/handler/Register.h>
#include <sequencer/handler/track/CreateTrackHandler.h>
#include <sequencer/handler/track/UpdateTrackHandler.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace fr::musician::data_definition;
using namespace boost::unit_test;
using namespace google::protobuf::util;


BOOST_AUTO_TEST_SUITE(UpdateTrackHandlerTests)


class UpdateTrackHandlerTestsFixture {
  public:
    UpdateTrackHandlerTestsFixture() {
      track::registerCreateTrackHandler(registry, tracks);
      track::registerUpdateTrackHandler(registry, tracks);
    }


    tracks_type tracks;
    FunctionRegistry registry;
};


BOOST_FIXTURE_TEST_CASE(
  update_track_handler_test,
  UpdateTrackHandlerTestsFixture,
  *description(
    "The update track handler should update an existing track"))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_createtrack()->mutable_track()->set_name("Test track");
  Response response;
  registry.function(Request::DataCase::kCreateTrack)(request, response);

  *request.mutable_updatetrack()->mutable_track() =
    response.createtrack().track();
  request.mutable_updatetrack()->mutable_track()->set_name("Test new");
  registry.function(Request::DataCase::kUpdateTrack)(request, response);
  BOOST_ASSERT(MessageDifferencer::Equals(
    request.updatetrack().track(),
    response.updatetrack().track()));

  const auto track = tracks.container.find(request.updatetrack().track().id());
  BOOST_ASSERT(MessageDifferencer::Equals(
    request.updatetrack().track(),
    track->second.attributes));
}


BOOST_FIXTURE_TEST_CASE(
  update_track_handler_negative_test,
  UpdateTrackHandlerTestsFixture,
  *description(
    "The update track handler should return an error if the track does not"
    "exist."))
{
  boost::debug::detect_memory_leaks(false);

  Request request;
  request.mutable_updatetrack()->mutable_track()->set_id(42);
  Response response;
  registry.function(Request::DataCase::kUpdateTrack)(request, response);

  BOOST_REQUIRE_EQUAL(response.has_error(), true);
  BOOST_REQUIRE_EQUAL(
    response.error().errorcode(),
    Error::ErrorCode::Error_ErrorCode_TRACK_DOES_NOT_EXIST);
}


BOOST_AUTO_TEST_SUITE_END()
