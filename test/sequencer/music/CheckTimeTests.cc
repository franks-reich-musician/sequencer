#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sequencer/music/Time.h>
#include <sequencer/music/Exceptions.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::time;
using namespace fr::musician::data_definition::time;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(CheckTimeTests)


BOOST_AUTO_TEST_CASE(
  check_time_positive_test,
  *description(
    "Check music time should return without throwing if the music time is"
    "formed correctly."))
{
  boost::debug::detect_memory_leaks(false);

  MusicTime time;
  time.set_resolution(1024);
  for (size_t i = 0; i < 6; i++) {
    time.add_denominators((unsigned int) pow(2, i));
  }

  time.add_divisions(1);
  time.add_divisions(2);
  time.add_divisions(5);
  time.add_divisions(2);
  time.add_divisions(1);
  time.add_divisions(3);
  time.set_remainder(0);

  BOOST_REQUIRE_NO_THROW(checkMusicTime(time));
}


BOOST_AUTO_TEST_CASE(
  check_time_denominator_and_division_count_negative_test,
  *description(
    "Check music time should throw a denominator and division count mismatch"
    "if the following inequality does not hold:"
    "length(denominator) == length(division)"))
{
  boost::debug::detect_memory_leaks(false);

  MusicTime time;
  time.set_resolution(1024);
  for (size_t i = 0; i < 6; i++) {
    time.add_denominators((unsigned int) pow(2, i));
  }

  time.add_divisions(1);
  time.add_divisions(2);
  time.add_divisions(5);
  time.add_divisions(2);
  time.set_remainder(0);

  BOOST_REQUIRE_EXCEPTION(
    checkMusicTime(time),
    denominator_and_division_count_mismatch,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_CASE(
  check_time_denominators_negative_test,
  *description(
    "The check music time should throw a denominator error if the denominators"
    "are not of the form denominators[1]^k."))
{
  boost::debug::detect_memory_leaks(false);

  MusicTime time;
  time.set_resolution(1024);
  for (size_t i = 0; i < 6; i++) {
    time.add_denominators((unsigned int) pow(2, i));
  }

  time.set_denominators(3, 5);

  time.add_divisions(1);
  time.add_divisions(2);
  time.add_divisions(5);
  time.add_divisions(2);
  time.add_divisions(1);
  time.add_divisions(3);
  time.set_remainder(0);

  BOOST_REQUIRE_EXCEPTION(
    checkMusicTime(time),
    denominator_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_CASE(
  check_time_resolution_negative_test,
  *description(
    "The check music time should throw a resolution error if the resolution"
    "is not of the form denominators[1]^k."))
{
  boost::debug::detect_memory_leaks(false);

  MusicTime time;
  time.set_resolution(1012);
  for (size_t i = 0; i < 6; i++) {
    time.add_denominators((unsigned int) pow(2, i));
  }

  time.add_divisions(1);
  time.add_divisions(2);
  time.add_divisions(5);
  time.add_divisions(2);
  time.add_divisions(1);
  time.add_divisions(3);
  time.set_remainder(0);

  BOOST_REQUIRE_EXCEPTION(
    checkMusicTime(time),
    resolution_error,
    [](const exception &ex) { return true; });
}


BOOST_AUTO_TEST_SUITE_END()
