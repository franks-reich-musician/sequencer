#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <google/protobuf/util/message_differencer.h>


#include <sequencer/music/Time.h>
#include <sequencer/music/Exceptions.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::time;
using namespace fr::musician::data_definition::time;
using namespace boost::unit_test;
using namespace google::protobuf::util;
using namespace std;


BOOST_AUTO_TEST_SUITE(ConvertInternalTimeTests)


BOOST_AUTO_TEST_CASE(
  convert_internal_time_to_music_time_base_two_all_denominators,
  *description(
    "Convert internal time should convert the internal time to the music time"
    "with the base 2, 11 denominators and resolution 2048."))
{
  boost::debug::detect_memory_leaks(false);

  const auto actualMusicTime = convertInternalTime(6321, 2, 11, 2048);

  MusicTime expectedMusicTime;
  expectedMusicTime.set_resolution(2048);
  for (size_t i = 0; i < 11; i++) {
    expectedMusicTime.add_denominators((unsigned int) pow(2, i));
  }

  expectedMusicTime.add_divisions(3);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.set_remainder(1);

  BOOST_REQUIRE_EQUAL(
    expectedMusicTime.remainder(),
    actualMusicTime.remainder());
  BOOST_REQUIRE(MessageDifferencer::Equals(actualMusicTime, expectedMusicTime));
}


BOOST_AUTO_TEST_CASE(
  convert_internal_time_to_music_time_base_two_seven_denominators,
  *description(
    "Convert internal time should convert the internal time to the music time"
    "with the base 2, 7 denominators and resolution 2048."))
{
  boost::debug::detect_memory_leaks(false);

  const auto actualMusicTime = convertInternalTime(6321, 2, 7, 2048);

  MusicTime expectedMusicTime;
  expectedMusicTime.set_resolution(2048);
  for (size_t i = 0; i < 7; i++) {
    expectedMusicTime.add_denominators((unsigned int) pow(2, i));
  }

  expectedMusicTime.add_divisions(3);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.set_remainder(17);

  BOOST_REQUIRE_EQUAL(
    expectedMusicTime.remainder(),
    actualMusicTime.remainder());
  BOOST_REQUIRE(MessageDifferencer::Equals(actualMusicTime, expectedMusicTime));
}


BOOST_AUTO_TEST_CASE(
  convert_internal_time_to_music_time_base_three_all_denominators,
  *description(
    "Convert internal time should convert the internal time to the music time"
    "with the base 3, 9 denominators and resolution 19683."))
{
  boost::debug::detect_memory_leaks(false);

  const auto actualMusicTime = convertInternalTime(38402, 3, 9, 19683);

  MusicTime expectedMusicTime;
  expectedMusicTime.set_resolution(19683);
  for (size_t i = 0; i < 9; i++) {
    expectedMusicTime.add_denominators((unsigned int) pow(3, i));
  }

  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.set_remainder(2);

  BOOST_REQUIRE(MessageDifferencer::Equals(actualMusicTime, expectedMusicTime));
}


BOOST_AUTO_TEST_CASE(
  convert_internal_time_to_music_time_base_three_six_denominators,
  *description(
    "Convert internal time should convert the internal time to the music time"
    "with the base 3, 6 denominators and resolution 19683."))
{
  boost::debug::detect_memory_leaks(false);

  const auto actualMusicTime = convertInternalTime(38402, 3, 6, 19683);

  MusicTime expectedMusicTime;
  expectedMusicTime.set_resolution(19683);
  for (size_t i = 0; i < 6; i++) {
    expectedMusicTime.add_denominators((unsigned int) pow(3, i));
  }

  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.add_divisions(1);
  expectedMusicTime.add_divisions(2);
  expectedMusicTime.add_divisions(0);
  expectedMusicTime.set_remainder(8);

  BOOST_REQUIRE_EQUAL(
    expectedMusicTime.remainder(),
    actualMusicTime.remainder());
  BOOST_REQUIRE(MessageDifferencer::Equals(actualMusicTime, expectedMusicTime));
}


BOOST_AUTO_TEST_SUITE_END()
