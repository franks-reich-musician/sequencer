#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>


#include <sequencer/music/Time.h>
#include <sequencer/music/Exceptions.h>


using namespace fr::musician::sequencer;
using namespace fr::musician::sequencer::time;
using namespace fr::musician::data_definition::time;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(ConvertMusicTimeTests)


BOOST_AUTO_TEST_CASE(
  convert_music_time_to_internal_time_test,
  *description(
    "Convert music time should convert the music time to the internal time."))
{
  boost::debug::detect_memory_leaks(false);

  MusicTime time;
  time.set_resolution(1024);
  for (size_t i = 0; i < 6; i++) {
    time.add_denominators((unsigned int) pow(2, i));
  }

  time.add_divisions(1);
  time.add_divisions(2);
  time.add_divisions(5);
  time.add_divisions(2);
  time.add_divisions(1);
  time.add_divisions(3);
  time.set_remainder(5);

  const auto actual = convertMusicTime(time);
  BOOST_REQUIRE_EQUAL(actual, 3749);
}


BOOST_AUTO_TEST_SUITE_END()
